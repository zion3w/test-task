<?php
if (!isset($_POST['chat-form-text'])) {
	die();
}

session_start();

$postArr = [];

foreach ($_POST as $key => $value) {

	$key = filter_var($key, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);
	$value = filter_var($value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	$postArr[$key] = $value;
}

$user = $_SESSION['user'];


require_once __DIR__ . '/php/helper.php';
$helper = new Helper();

require (__DIR__ . '/config.php');


// chat table
$dbTableName = 'chat';
/*
id | uid | message | time_stamp
*/
$chatMessage = [
	'uid' => $user['id'],
	'uname' => $user['name'],
	'message' => $postArr['chat-form-text'],
];

try {

	$db->insertInto($dbTableName, $chatMessage);

	header('Content-Type: application/json');
	echo json_encode(['resp'=>'ok']);

} catch (Exception $e) {
	http_response_code(400);
	echo 'insertInto error Exception: ' . $e->getMessage() . "\n";
}
