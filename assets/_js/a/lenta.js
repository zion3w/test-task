/*
- Based on https://blog.codepen.io/2018/01/15/build-serverless-blog-codepen/#toc-front-end
*/

// Your web app's Firebase configuration
var firebaseConfig = {
	apiKey: 'AIzaSyDXzuoc2K3I2IohPiFAEVsksotL0uPogj0',
	authDomain: 'lenta-stream.firebaseapp.com',
	databaseURL: 'https://lenta-stream.firebaseio.com',
	projectId: 'lenta-stream',
	storageBucket: '',
	messagingSenderId: '121905761202',
	appId: '1:121905761202:web:69d9f821dcd0608f'
};
// Initialize Firebase
if (!firebase.apps.length) {
	firebase.initializeApp(firebaseConfig);
}


var vueApp = new Vue({
	// el: '#vue-el',
	http: {
		// root: 'https://us-central1-tabi-1266.cloudfunctions.net/app',
		// root: 'https://europe-west2.cloudfunctions.net/app',
		root: 'https://us-central1-lenta-stream.cloudfunctions.net/',
		emulateJSON: true
	},
	data: {
		user: null,
		message: 'Загрузка...',
		formMessageHtml: false,
		articleList: [],
		readingArticleKey: '',
		newPost: {
			title: '',
			content: ''
		}
	},
	filters: {
		date: function(val) {
			var date = new Date(val);
			return date.getDate()+'.'+date.getMonth()+'.'+date.getFullYear()+' '+date.getHours()+':'+(date.getMinutes() > 0 ? date.getMinutes() : '00');
		},
		content: function(article) {
			var content;
			if (article.key === vueApp.readingArticleKey) {
				content = article.content;
				// location.hash = 'news-' + article.key;
			}else{
				content = article.content.substring(0, 150) + '...';
			}
			return content;
		}
	},
	mounted: function(){
		this.loadPosts();
		// react to sign in event
		firebase.auth().onAuthStateChanged(function(user) {
			vueApp.user = user;
			if (user) {
				vueApp.$nextTick(function () {
					// console.log(vueApp.$el.querySelector('.lenta_user_form form'));
					// this.$root.$el.className = '';
					// jformInit(vueApp.$el.querySelector('.lenta_user_form form'));
					// console.log(this.$root.$el);
					autosize(vueApp.$el.querySelector('.lenta_user_form textarea'));
				});
			}
		});

	},
	// updated: function () {
	// 	this.$nextTick(function () {
	// 		will fired on each change, even on input change
	// 	})
	// },
	methods: {
		signIn: function() {
			var provider = new firebase.auth.GoogleAuthProvider();
			// allow user to select account to log in everytime
			provider.setCustomParameters({ 'prompt': 'select_account' });
			firebase.auth().signInWithPopup(provider);
		},
		signOut: function() {
			firebase.auth().signOut();
			this.user = null;
		},
		loadPosts: function() {
			this.articleList = [];
			this.$http.get('posts')
			.then(function(res) {
				if (!res.data) { return; }
				var keys = Object.keys(res.data);
				for(var i = keys.length - 1; i > -1; i--){
					var key = keys[i];
					var article = res.data[key];
					article.key = key;
					vueApp.articleList.push(article);
				}
				vueApp.message = '';
			});
		},
		clearNewPost: function() {
			this.newPost.title = '';
			this.newPost.content = '';
		},
		addPost: function() {

			if (!this.user) {
				this.message = 'Войдите на сайт...';
				return;
			}

			this.$refs.newPostSubmitButton.disabled = true;
			if (!formValidate(this.$refs.newPostForm)) {
				this.$refs.newPostSubmitButton.disabled = false;
				return;
			}

			firebase.auth().currentUser.getIdToken(true)
			.then(function(idToken) {
				vueApp.$http.post('posts', vueApp.newPost, { headers: { 'Authorization': 'Bearer ' + idToken } })
				.then(function(res) {
					vueApp.articleList.unshift(res.data);
					vueApp.clearNewPost();
					this.$refs.newPostSubmitButton.disabled = false;
					this.formMessageHtml = true;
				});
			});
		},
		hideFormMessageHtml: function () {
			this.formMessageHtml = false;
		}
	}
});



var vueState = false;

document.addEventListener('turbolinks:load', function(event) {
	vueEl = qs('#vue-el');
	if (vueEl && !vueState) {
		vueApp.$mount('#vue-el');
		vueState = true;
	} else if(vueEl && vueState){
		vueApp.$mount('#vue-el');
		vueEl.innerHTML = '';
		vueEl.appendChild(vueApp.$el);
	}
});
document.addEventListener('turbolinks:before-cache', function() {
	if (qs('#vue-el')) {
		vueApp.$destroy();
	}
});


