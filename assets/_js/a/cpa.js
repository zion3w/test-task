'use strict';

function randomNum(min, max) {
	return Math.floor(Math.random() * (max - min + 1) ) + min;
}

var objCPA;


function readJSON(path) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', path, true);
	xhr.responseType = 'json';
	xhr.onload = function(e) {
		if (this.status === 200 && this.readyState === 4) {
			objCPA = this.response;
			advCPA(this.response);
		}
	};
	xhr.send();
}

// readJSON('/_html/cpa/');
// readJSON('/_html/cpa/index.json');

// readJSON('https://z3w.host/cpa/');
readJSON('https://z3w.host/cpa/index.json');


function setHtmlCPA(arg) {

	var cpaBlock = document.querySelector('#cpa');

	if (!cpaBlock) {
		cpaBlock = document.createElement('DIV');
		cpaBlock.setAttribute('id', 'cpa');

		document.querySelector('#side_a').appendChild(cpaBlock);
		// if (document.documentElement.clientWidth > 1280) {
		// 	document.querySelector('#side_a').appendChild(cpaBlock);
		// } else if (document.documentElement.clientWidth > 1024){
		// 	document.querySelector('#side_a').appendChild(cpaBlock);
		// } else {
		// 	document.querySelector('#side_a').appendChild(cpaBlock);
		// }
	}


	cpaBlock.className = '';
	// cpaBlock.innerHTML = '';

	var strHtmlCPA = '';
	// strHtmlCPA += '<p class="tal w100">Наши спонсоры:</p>';
	strHtmlCPA += '<a class="cpa_url" href="'+arg['url']+'" rel="nofollow" target="_blank">';
	strHtmlCPA += '<p class="cpa_title">'+arg['title']+'</p>';
	strHtmlCPA += '<div class="cpa_img"><img src="'+arg['images'][randomNum(0, arg['images'].length-1)]+'" alt="'+arg['title']+'">';
	if (Array.isArray(arg['desc'])) {
		strHtmlCPA += '<ul class="cpa_desc">';
		for(var i = 0, l = arg['desc'].length; i < l; i++){
			strHtmlCPA += '<li>'+arg['desc'][i]+'</li>';
		}
		strHtmlCPA += '</ul>';
	} else {
		strHtmlCPA += '<span class="cpa_desc">'+arg['desc']+'</span>';
	}
	strHtmlCPA += '</div>';
	strHtmlCPA += '</a>';
	strHtmlCPA += '<div class="cpa_contact"><label class="btn" for="contact-form">Объявления</label></div>';


	cpaBlock.innerHTML = strHtmlCPA;
	setTimeout(function() {
		if (document.documentElement.clientHeight > cpaBlock.clientHeight) {
			// cpaBlock.style.top = document.documentElement.clientHeight - cpaBlock.clientHeight -25 + 'px';
			cpaBlock.style.top = document.documentElement.clientHeight - cpaBlock.clientHeight + 'px';
			cpaBlock.className += 'sticky visible';
		} else{
			cpaBlock.className += 'visible';
		}
	}, 350);
}
// show cpa-adv from obj
function advCPA(argObjCPA) {

	var currUrl = location.pathname;

	var showOtherUrl = true;

	for (var key in argObjCPA) {
		if (currUrl.indexOf(key) !== -1) {
		// if (currUrl.indexOf('/'+key) !== -1) {
			var arrItemCPA = argObjCPA[key];

			var cpaItemNum = 0;

			if (arrItemCPA.length > 1) {
				cpaItemNum = randomNum(0, arrItemCPA.length-1);
			}

			if (arrItemCPA[cpaItemNum]['no-mobile'] && document.querySelector('html.touchevents')) {
				showOtherUrl = true;
				break;
			}

			setHtmlCPA(arrItemCPA[cpaItemNum]);

			showOtherUrl = false;

			break;
		}
	}


	if (showOtherUrl) {
		var argObjCPA_keysArr = Object.keys(argObjCPA);
		var argObjCPA_key = argObjCPA_keysArr[randomNum(0, argObjCPA_keysArr.length-1)];

		var argObjCPA_new = argObjCPA[argObjCPA_key][randomNum(0, argObjCPA[argObjCPA_key].length-1)];

		if (argObjCPA_new['no-mobile'] && document.querySelector('html.touchevents')) {
			advCPA(argObjCPA);
		} else {
			setHtmlCPA(argObjCPA_new);
		}

	}


}


if (Turbolinks.supported) {

	// document.addEventListener('turbolinks:render', function(e) {
	document.addEventListener('turbolinks:load', function(e) {
		if (objCPA !== 'undefined') {
			advCPA(objCPA);
		}

	});

	document.addEventListener('turbolinks:before-cache', function() {
		document.querySelector('#cpa').className = '';
		// document.querySelector('#cpa').innerHTML = '';
	});

}
// else
// {
// 	document.addEventListener('DOMContentLoaded', function(event) {
// 		advCPA(objCPA);
// 	});
// }