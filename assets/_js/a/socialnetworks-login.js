'use strict';
/* Google with callback */
var auth2;

loadScript('//apis.google.com/js/platform.js', function(error, script) {
	if (error) {
		console.log(error);
	} else {
		// console.log(gapi);
		gapi.load('auth2', function(){

			var clientIdGoogle = qs('[data-gform-client-id]').getAttribute('data-gform-client-id');

			gapi.auth2.init({
				client_id: clientIdGoogle,
				cookiepolicy: 'single_host_origin'
			}).then(function() {
				auth2 = gapi.auth2.getAuthInstance();
			});
		});

		if (qs('[data-gform-client-id]')) {
			qs('[data-gform-client-id]').addEventListener('click', _modalLogin_Google);
		}
	}
});

function _modalLogin_Google() {

	auth2.grantOfflineAccess({
		scope: 'profile email',
		redirect_uri: 'postmessage'
	}).then(function(resp) {

		_submitSocialLoginForm('userCodeGoogle', resp.code);

	}).catch(function(err) {
		console.log(err);
	});

}
/* Google with callback /*/

/* FB with callback */
var responseFB;
var fbRerequestEmail;

loadScript('//connect.facebook.net/'+qs('html').getAttribute('lang')+'/sdk.js', function(error, script) {
	if (error) {
		console.log(error);
	} else {

		FB.init({
			appId: qs('[data-fbform-client-id]').getAttribute('data-fbform-client-id'),
			version: 'v3.3',
			cookie: true,
			xfbml: false
		});

		FB.getLoginStatus(function(response){
			responseFB = response;
		});


		if (qs('[data-fbform-client-id]')) {
			qs('[data-fbform-client-id]').addEventListener('click', _modalLogin_FB);
		}

	}
});

function _modalLogin_FB() {

	var fbConfig = {
		scope: 'public_profile,email',
		return_scopes: true
	}
	if (fbRerequestEmail) {
		fbConfig.auth_type = 'rerequest';
	}

	FB.login(function(response){
		responseFB = response;
		if (responseFB.status === 'connected') {

			if (responseFB.authResponse.grantedScopes.indexOf('email') !== -1) {

				FB.api('/me', {fields: 'email'}, function(response) {
					if (!response.email) {

						FB.api('/me/permissions', 'delete', {});
						responseFB.status = 'not_authorized';

						// alert('Profile has no email: '+response.email);
						alert(qs('.login_socialnetworks_fb [data-fbform-error-email-empty]').innerText);

					} else {
						_submitSocialLoginForm('userIDFB', responseFB.authResponse.userID);
					}
				});

			} else {

				fbRerequestEmail = true;
				// FB.api('/me/permissions', 'delete', {});
				responseFB.status = 'not_authorized';

				// alert('Your e-mail is required to ensure the proper functioning of the Website and its services');
				alert(qs('.login_socialnetworks_fb [data-fbform-error-email-access]').innerText);

			}

		}
	}, fbConfig);

}

/* FB with callback /*/


function _submitSocialLoginForm(loginProvider, loginProviderResponse) {
	var form = document.createElement('form');
	var formFields;
	form.setAttribute('method', 'post');
	form.setAttribute('action', window.location.href);
	form.className = 'dn';
	formFields +='<input name="option" type="hidden" value="com_users">';
	formFields +='<input name="task" type="hidden" value="user.login">';
	formFields +='<input id="'+loginProvider+'" name="'+loginProvider+'" type="hidden" value="'+loginProviderResponse+'">';
	formFields +='<input name="return" type="hidden" value="'+btoa(window.location.href)+'">';
	formFields +='<input name="'+qs('[data-jform-token]').getAttribute('data-jform-token')+'" type="hidden" value="1">';
	form.innerHTML += formFields;
	document.body.appendChild(form);
	form.submit();

}




/* OLD VERSION OF A SCRIPT(without callbacks) */



// loadjscssfile('//apis.google.com/js/platform.js', 'js');
// loadjscssfile('//connect.facebook.net/'+qs('html').getAttribute('lang')+'/sdk.js', 'js');

// function _submitSocialLoginForm(loginProvider, loginProviderResponse) {
// 	var form = document.createElement('form');
// 	var formFields;
// 	form.setAttribute('method', 'post');
// 	form.setAttribute('action', window.location.href);
// 	form.className = 'dn';
// 	formFields +='<input name="option" type="hidden" value="com_users">';
// 	formFields +='<input name="task" type="hidden" value="user.login">';
// 	formFields +='<input id="'+loginProvider+'" name="'+loginProvider+'" type="hidden" value="'+loginProviderResponse+'">';
// 	formFields +='<input name="return" type="hidden" value="'+btoa(window.location.href)+'">';
// 	formFields +='<input name="'+qs('[data-jform-token]').getAttribute('data-jform-token')+'" type="hidden" value="1">';
// 	form.innerHTML += formFields;
// 	document.body.appendChild(form);
// 	form.submit();

// }

/* Google */
// var auth2;

// gapi.load('auth2', function(){

// 	var clientIdGoogle = qs('[data-gform-client-id]').getAttribute('data-gform-client-id');

// 	gapi.auth2.init({
// 		client_id: clientIdGoogle,
// 		cookiepolicy: 'single_host_origin'
// 	}).then(function() {
// 		auth2 = gapi.auth2.getAuthInstance();
// 	});
// });

// function _modalLogin_Google() {

// 	auth2.grantOfflineAccess({
// 		scope: 'profile email',
// 		redirect_uri: 'postmessage'
// 	}).then(function(resp) {

// 		_submitSocialLoginForm('userCodeGoogle', resp.code);

// 	}).catch(function(err) {
// 		console.log(err);
// 	});

// }

// if (qs('[data-gform-client-id]')) {
// 	qs('[data-gform-client-id]').addEventListener('click', _modalLogin_Google);
// }
/* Google /z*/


/* FB */
// var responseFB;
// var fbRerequestEmail;

// FB.init({
// 	appId: qs('[data-fbform-client-id]').getAttribute('data-fbform-client-id'),
// 	version: 'v3.3',
// 	cookie: true,
// 	xfbml: false
// });

// FB.getLoginStatus(function(response){
// 	responseFB = response;
// });


// function _modalLogin_FB() {

// 	var fbConfig = {
// 		scope: 'public_profile,email',
// 		return_scopes: true
// 	}
// 	if (fbRerequestEmail) {
// 		fbConfig.auth_type = 'rerequest';
// 	}

// 	FB.login(function(response){
// 		responseFB = response;
// 		if (responseFB.status === 'connected') {

// 			if (responseFB.authResponse.grantedScopes.indexOf('email') !== -1) {

// 				FB.api('/me', {fields: 'email'}, function(response) {
// 					if (!response.email) {

// 						FB.api('/me/permissions', 'delete', {});
// 						responseFB.status = 'not_authorized';

// 						// alert('Profile has no email: '+response.email);
// 						alert(qs('.login_socialnetworks_fb [data-fbform-error-email-empty]').innerText);

// 					} else {
// 						_submitSocialLoginForm('userIDFB', responseFB.authResponse.userID);
// 					}
// 				});

// 			} else {

// 				fbRerequestEmail = true;
// 				// FB.api('/me/permissions', 'delete', {});
// 				responseFB.status = 'not_authorized';

// 				// alert('Your e-mail is required to ensure the proper functioning of the Website and its services');
// 				alert(qs('.login_socialnetworks_fb [data-fbform-error-email-access]').innerText);

// 			}

// 		}
// 	}, fbConfig);

// }

// if (qs('[data-fbform-client-id]')) {
// 	qs('[data-fbform-client-id]').addEventListener('click', _modalLogin_FB);
// }




/* FB /z*/

