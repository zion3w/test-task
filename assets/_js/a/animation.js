'use strict';
// to do: make some object/array for stoping animation ?
// or just use - document.body.setAttribute('data-turbolinks', 'false')

function z3w_Anim(arg) {
	document.body.setAttribute('data-turbolinks', 'false');

	// ——————————————————————————————————————————————————
	// scrambleText
	// ——————————————————————————————————————————————————
	function scrambleText(t,e){function n(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}if(document.querySelector(t)){var r=e,a=new(function(){function t(e){n(this,t),this.el=e,this.chars="!<>-_\\/[]{}—=+*^?#@!_",this.update=this.update.bind(this)}return t.prototype.setText=function(t){var e=this,n=this.el.innerText,r=Math.max(n.length,t.length),a=new Promise(function(t){return e.resolve=t});this.queue=[];for(var i=0;i<r;i++){var h=n[i]||"",s=t[i]||"",o=Math.floor(40*Math.random()),u=o+Math.floor(40*Math.random());this.queue.push({from:h,to:s,start:o,end:u})}return cancelAnimationFrame(this.frameRequest),this.frame=0,this.update(),a},t.prototype.update=function(){for(var t="",e=0,n=0,r=this.queue.length;n<r;n++){var a=this.queue[n],i=a.from,h=a.to,s=a.start,o=a.end,u=a.char;this.frame>=o?(e++,t+=h):this.frame>=s?((!u||Math.random()<.28)&&(u=this.randomChar(),this.queue[n].char=u),t+='<span class="dud">'+u+"</span>"):t+=i}this.el.innerHTML=t,e===this.queue.length?this.resolve():(this.frameRequest=requestAnimationFrame(this.update),this.frame++)},t.prototype.randomChar=function(){return this.chars[Math.floor(Math.random()*this.chars.length)]},t}())(document.querySelector(t)),i=0;!function t(){a.setText(r[i]).then(function(){setTimeout(t,2500)}),i=(i+1)%r.length}()}}
	// ——————————————————————————————————————————————————
	// scrambleText /z
	// ——————————————————————————————————————————————————


	if (document.body.clientWidth > 1050) {

		var scrambleTextArr = [];
		var scrambleTextArrHtml = qsa('.scrambletext.bottom .dn li');
		for(var i = 0, l = scrambleTextArrHtml.length; i < l; i++){
			scrambleTextArr.push(scrambleTextArrHtml[i].innerHTML);
		}
		scrambleText('.scrambletext.bottom', scrambleTextArr);

		if(qs('.tipping_textjs')){
			var load_text = navigator.userAgent + '<br>' + navigator.languages + '`<br>`';
			load_text += '`Loading...<br>`^1000 ███████████████ `[100%]<br>`^2000 `data initialization...<br>`^2000';
			load_text += Date();
			var typed = new Typed('.tipping_textjs', {
				strings: [load_text],
				startDelay: 3000,
				typeSpeed: 25,
				showCursor: false,
			});
		}
		if(qs('.tipping_text')){
			var typed = new Typed('.tipping_text', {
				stringsElement: '.tipping_text_orig',
				startDelay: 20000,
				typeSpeed: 100,
				backSpeed: 10,
				autoInsertCss: false
			});
		}
	}





	// ——————————————————————————————————————————————————
	// canvas space animation
	// ——————————————————————————————————————————————————
	function canvasSpace(canvasEl) {
		// if (!document.querySelector(canvasEl)) {
		// 	return;
		// }

		var canvas = document.querySelector(canvasEl);

		var stars_count = 100;
		if (canvas.parentNode.clientWidth < 1050) {
			stars_count = 50;
		}

		var ctx = canvas.getContext( '2d' ),
		fl = 300,
		count = stars_count, // stars amount
		points = [],
		startSpeed = 0,
		// tick = 0,
		width,
		height,
		bounds,
		vp,
		mouse,
		canvasOffset;

		function rand( min, max ) {
			return Math.random() * ( max - min ) + min;
		}

		function norm( val, min, max ) {
			return ( val - min ) / ( max - min );
		}

		function resetPoint( p, init ) {
			p.z = init ? rand( 0, bounds.z.max ) : bounds.z.max;
			p.x = rand( bounds.x.min, bounds.x.max ) / ( fl / ( fl + p.z ) );
			p.y = rand( bounds.y.min, bounds.y.max ) / ( fl / ( fl + p.z ) );
			p.ox = p.x;
			p.oy = p.y;
			p.oz = p.z;
			p.vx = 0;
			p.vy = 0;
			p.vz = rand( -1, -10 ) + startSpeed;
			p.ax = 0;
			p.ay = 0;
			p.az = 0;
			p.s = 0;
			p.sx = 0;
			p.sy = 0;
			p.os = p.s;
			p.osx = p.sx;
			p.osy = p.sy;
			p.hue = rand( 120, 200 );
			p.lightness = rand( 70, 100 );
			p.alpha = 0;
			return p;
		}

		function create() {
			vp = {
				x: width / 2,
				y: height / 2
			};
			mouse = {
				x: vp.x,
				y: vp.y,
				down: false
			};
			bounds = {
				x: { min: -vp.x, max: width - vp.x },
				y: { min: -vp.y, max: height - vp.y },
				z: { min: -fl, max: 1000 }
			};
		}

		function update() {
			if( mouse.down ) {
				if( startSpeed > -30 ) {
					startSpeed -= 0.1;
				} else {
					startSpeed = -30;
				}
			} else {
				if( startSpeed < 0 ) {
					startSpeed += 1;
				} else {
					startSpeed = 0;
				}
			}

			vp.x += ( ( width / 2 - ( mouse.x - width / 2 ) ) - vp.x ) * 0.025;
			vp.y += ( ( height / 2 - ( mouse.y - height / 2 ) ) - vp.y ) * 0.025;
			bounds = {
				x: { min: -vp.x, max: width - vp.x },
				y: { min: -vp.y, max: height - vp.y },
				z: { min: -fl, max: 1000 }
			};

			if( points.length < count ) {
				points.push( resetPoint( {} ) );
			}
			var i = points.length;
			while( i-- ) {
				var p = points[ i ];
				p.vx += p.ax;
				p.vy += p.ay;
				p.vz += p.az;
				p.x += p.vx;
				p.y += p.vy;
				p.z += p.vz;
				if( mouse.down ) {
					p.az = -0.5;
				}
				if(
					p.sx - p.sr > bounds.x.max ||
					p.sy - p.sr > bounds.y.max ||
					p.z > bounds.z.max ||
					p.sx + p.sr < bounds.x.min ||
					p.sy + p.sr < bounds.y.min ||
					p.z < bounds.z.min
					) {
					resetPoint( p );
					// indent fix
				}
				p.ox = p.x;
				p.oy = p.y;
				p.oz = p.z;
				p.os = p.s;
				p.osx = p.sx;
				p.osy = p.sy;
			}
		}

		function render() {
			ctx.save();
			ctx.translate( vp.x, vp.y );
			ctx.clearRect( -vp.x, -vp.y, width, height );
			var i = points.length;
			while( i-- ) {
				var p = points[ i ];
				p.s = fl / ( fl + p.z );
				p.sx = p.x * p.s;
				p.sy = p.y * p.s;
				p.alpha = ( bounds.z.max - p.z ) / ( bounds.z.max / 2 );
				ctx.beginPath();
				ctx.moveTo( p.sx, p.sy );
				ctx.lineTo( p.osx, p.osy );
				ctx.lineWidth = 2;
				ctx.strokeStyle = 'hsla(' + p.hue + ', 100%, ' + p.lightness + '%, ' + p.alpha + ')';
				ctx.stroke();
			}
			ctx.restore();
		}

		function resize() {
			width = canvas.width = canvas.parentNode.clientWidth;
			height = canvas.height = canvas.parentNode.clientHeight;
			canvasOffset = { x: canvas.offsetLeft, y: canvas.offsetTop };
		}

		function mousemove( e ) {
			mouse.x = e.pageX - canvasOffset.x;
			mouse.y = e.pageY - canvasOffset.y;
		}

		function mousedown() {
			mouse.down = true;
		}

		function mouseup() {
			mouse.down = false;
		}

		function loop() {
			// @todo: тут останавливать через иф какой нить
			update();
			render();
			requestAnimationFrame( loop ); // вот это
			// tick++;
		}

		window.addEventListener( 'resize', resize );
		canvas.parentNode.addEventListener( 'mousemove', mousemove );
		canvas.addEventListener( 'mousedown', mousedown );
		canvas.addEventListener( 'mouseup', mouseup );
		resize();
		create();
		loop();
	}
	if (qs('#canva-space')) {
		canvasSpace('#canva-space');
	}

	// ——————————————————————————————————————————————————
	// canvas space animation /z
	// ——————————————————————————————————————————————————



	// ——————————————————————————————————————————————————
	// Canvas sphere-in-sphere animation /z
	// ——————————————————————————————————————————————————

	function canvasSphere(canvasId) {

		// if (!document.querySelector(canvasId)) {
		// 	return;
		// }

		// var canvas = document.getElementById(canvasId);
		var canvas = document.querySelector(canvasId);
		var ctx = canvas.getContext('2d');

		var cw = (canvas.width = canvas.parentNode.clientWidth),
		// var cw = (canvas.parentNode.clientWidth),
		cx = cw / 2;
		var ch = (canvas.height = canvas.parentNode.clientHeight),
		// var ch = (canvas.parentNode.clientHeight),
		cy = ch / 2;

		var m = { x: 0, y: 0 };
		var target = { x: 0, y: 0 };
		var speed = 0.0005;
		var easing = 0.90;

		var frames = 0;

		var balls = [];
		var vp = { x: cx, y: cy }; //vanishing point
		var fl = 200; // focal length

		function Ball(R) {

			this.R = R;
			this.r = .04*this.R;

			// 3D position
			this.pos = spherePointPicking(this.R)

			// 2D position
			this.x = this.pos.x + cx;
			this.y = this.pos.y + cy;
			this.a = { x: 0, y: 0 };
			this.scale = { x: 1, y: 1 };
			this.c = oGrd(this.r/2, 210);

			this.rotateX = function(angle) {
				var cos = Math.cos(angle);
				var sin = Math.sin(angle);
				var y1 = this.pos.y * cos - this.pos.z * sin;
				var z1 = this.pos.z * cos + this.pos.y * sin;

				this.pos.y = y1;
				this.pos.z = z1;
			};

			this.rotateY = function(angle) {
				var cos = Math.cos(angle);
				var sin = Math.sin(angle);
				var x1 = this.pos.x * cos - this.pos.z * sin;
				var z1 = this.pos.z * cos + this.pos.x * sin;

				this.pos.x = x1;
				this.pos.z = z1;
			};

			this.draw3D = function() {
				if (this.pos.z > -fl) {
					var scale = fl / (fl - this.pos.z);

					this.scale = { x: scale, y: scale };
					this.x = vp.x + this.pos.x * scale;
					this.y = vp.y + this.pos.y * scale;
					this.visible = true;
				} else {
					this.visible = false;
				}
			};

			this.draw2D = function() {

				ctx.save();
				ctx.translate(this.x, this.y);
				ctx.scale(this.scale.x, this.scale.y);
				ctx.beginPath();
				ctx.fillStyle = this.c;
				ctx.fillRect(0, 0, this.r, this.r);
				ctx.restore();
			};
		}

		var canva_globe_radius = canvas.parentNode.clientHeight/3;

		for (var i = 0; i < 75; i++) {
			balls.push(new Ball(canva_globe_radius));
			// balls.push(new Ball(240));
			// balls.push(new Ball(205));
			// balls.push(new Ball(150));
			// balls.push(new Ball(75));
			// balls.push(new Ball(50));
		}
		if (document.body.clientWidth > 1050){
			for (var i = 0; i < 100; i++) {
				balls.push(new Ball(206));
			}
		}

		function Draw() {
			var t = new Date().getTime() / 127;

			ctx.clearRect(0, 0, cw, ch);

			frames+=.1;
			//t = new Date().getTime() / 127;
			m.x = cx + Math.cos(t / 43 + Math.cos(t / 47 + frames)) * 8;
			m.y = cy + Math.sin(t / 31 + Math.cos(t / 37 + frames)) * 8;

			target.x = (m.y - vp.y) * speed;
			target.y = (m.x - vp.x) * speed;

			balls.map(function(b) {
				b.draw3D();
			});
			balls.sort(function(a, b) {
				return a.pos.z - b.pos.z;
			});

			target.x *= easing;
			target.y *= easing;

			balls.map(function(b) {
				b.rotateX(target.x);
				b.rotateY(target.y);
				if (b.visible) {
					b.draw2D();
				}
			});

			// @todo: add stop condition
			var requestId = window.requestAnimationFrame(Draw);
		}
		Draw();


		function oGrd(r, h) {
			var grd = ctx.createRadialGradient(r,r,0,r,r,r);

			grd.addColorStop(0, "hsla(" + h + ",95%,95%, 1)");
			grd.addColorStop(0.4, "hsla(" + h + ",95%,45%,.5)");
			grd.addColorStop(1, "hsla(" + h + ", 95%, 45%, 0)");

			return grd;
		}


		function spherePointPicking(R){
			//How to generate random points on a sphere:
			//https://math.stackexchange.com/questions/1585975/how-to-generate-random-points-on-a-sphere#1586185
			var u1 = Math.random();
			var u2 = Math.random();
			var s = Math.acos(2*u1 - 1) - Math.PI/2;
			var t = 2*Math.PI*u2;

			return {x : R * Math.cos(s) * Math.cos(t),
				y : R * Math.cos(s) * Math.sin(t),
				z : R * Math.sin(s)
			}
		}

	}

	if(qs('#canva-sphere')){
		canvasSphere('#canva-sphere')
	}

	// ——————————————————————————————————————————————————
	// Canvas sphere-in-sphere animation /z
	// ——————————————————————————————————————————————————

	// ——————————————————————————————————————————————————
	// Canvas matrix animation
	// ——————————————————————————————————————————————————

	function canvasMatrix(canvasID) {
		// var c = document.getElementById('matrix-canva');
		var c = document.querySelector(canvasID);
		var ctx = c.getContext('2d');

		//making the canvas full screen
		c.height = c.parentNode.clientHeight;
		c.width = c.parentNode.clientWidth;

		//chinese characters - taken from the unicode charset
		var chinese = '•◘○♀♪♫☼►◄↕‼§▬↨↑↓→←∟↔▼▲◙¶#$%&*Ъ+╝○xЖч▐!@#$%^&*➂⛓✆↭✇↳✈℞✉♛℡⚶⍶♻甾✎X⛯⍯᎗⌱⍙⚵᧽⛟⚛✓᎒✗⌖⚙࿏✦⚖⇌⚕༶畄™✧༖⅍߶↗⚠⬍❮⌆®⇅❯↨❨©↻❩¢£¥€©®~∇Δ™∅∑∏∉∈∋∀∂∃<>☈☏☒☣♄♃♹⛐⛬⚞⚟⚝☰☲☵☷';
		//converting the string into an array of single characters
		chinese = chinese.split('');

		var font_size = 16;
		var columns = c.width/font_size; //number of columns for the rain
		//an array of drops - one per column
		var drops = [];
		//x below is the x coordinate
		//1 = y co-ordinate of the drop(same for every drop initially)
		for(var x = 0; x < columns; x++){
			drops[x] = 1;
		}

		//drawing the characters
		function draw() {
			//Black BG for the canvas
			//translucent BG to show trail
			ctx.fillStyle = 'rgba(0, 0, 0, 0.07)';
			ctx.fillRect(0, 0, c.width, c.height);

			// ctx.fillStyle = '#090'; //green text
			// ctx.fillStyle = '#639d8a'; //green text
			ctx.fillStyle = '#45635e'; //green text
			ctx.font = font_size + 'px arial';
			//looping over drops
			for(var i = 0; i < drops.length; i++)
			{
				//a random chinese character to print
				var text = chinese[Math.floor(Math.random()*chinese.length)];
				//x = i*font_size, y = value of drops[i]*font_size
				ctx.fillText(text, i*font_size, drops[i]*font_size);

				//sending the drop back to the top randomly after it has crossed the screen
				//adding a randomness to the reset to make the drops scattered on the Y axis
				if(drops[i]*font_size > c.height && Math.random() > 0.975){
					drops[i] = 0;
				}

				//incrementing Y coordinate
				drops[i]++;
			}
		}

		setInterval(draw, 100);

	}

	if(qs('#canva-matrix')){
		canvasMatrix('#canva-matrix');
	}
	// ——————————————————————————————————————————————————
	// Canvas matrix animation /z
	// ——————————————————————————————————————————————————

	// ——————————————————————————————————————————————————
	// Canvas rainbow-grid animation
	// ——————————————————————————————————————————————————
	function canvasGrid(canvasGridEl) {

		var canvas,
		ctx,
		width,
		height,
		size,
		lines,
		tick;

		function line() {
			this.path = [];
			this.speed = rand( 10, 20 );
			this.count = randInt( 5, 20 );
			this.x = width / 2, + 1;
			this.y = height / 2 + 1;
			this.target = { x: width / 2, y: height / 2 };
			this.dist = 0;
			this.angle = 0;
			this.hue = tick / 5;
			this.life = 1;
			this.updateAngle();
			this.updateDist();
		}

		line.prototype.step = function( i ) {
			this.x += Math.cos( this.angle ) * this.speed;
			this.y += Math.sin( this.angle ) * this.speed;

			this.updateDist();

			if( this.dist < this.speed ) {
				this.x = this.target.x;
				this.y = this.target.y;
				this.changeTarget();
			}

			this.path.push( { x: this.x, y: this.y } );
			if( this.path.length > this.count ) {
				this.path.shift();
			}

			this.life -= 0.001;

			if( this.life <= 0 ) {
				this.path = null;
				lines.splice( i, 1 );
			}
		};

		line.prototype.updateDist = function() {
			var dx = this.target.x - this.x,
			dy = this.target.y - this.y;
			this.dist = Math.sqrt( dx * dx + dy * dy );
		}

		line.prototype.updateAngle = function() {
			var dx = this.target.x - this.x,
			dy = this.target.y - this.y;
			this.angle = Math.atan2( dy, dx );
		}

		line.prototype.changeTarget = function() {
			var randStart = randInt( 0, 3 );
			switch( randStart ) {
				case 0: // up
				this.target.y = this.y - size;
				break;
				case 1: // right
				this.target.x = this.x + size;
				break;
				case 2: // down
				this.target.y = this.y + size;
				break;
				case 3: // left
				this.target.x = this.x - size;
			}
			this.updateAngle();
		};

		line.prototype.draw = function( i ) {
			ctx.beginPath();
			var rando = rand( 0, 10 );
			for( var j = 0, length = this.path.length; j < length; j++ ) {
				ctx[ ( j === 0 ) ? 'moveTo' : 'lineTo' ]( this.path[ j ].x + rand( -rando, rando ), this.path[ j ].y + rand( -rando, rando ) );
			}
			ctx.strokeStyle = 'hsla(' + rand( this.hue, this.hue + 30 ) + ', 80%, 55%, ' + ( this.life / 3 ) + ')';
			ctx.lineWidth = rand( 0.1, 2 );
			ctx.stroke();
		};

		function rand( min, max ) {
			return Math.random() * ( max - min ) + min;
		}

		function randInt( min, max ) {
			return Math.floor( min + Math.random() * ( max - min + 1 ) );
		};

		var grid_size = 25;
		if (document.body.clientWidth > 1050){
			grid_size = 45;
		}

		function init() {
			canvas = document.querySelector( canvasGridEl );
			ctx = canvas.getContext( '2d' );
			size = grid_size;
			lines = [];
			reset();
			loop();
		}

		function reset() {
			width = Math.ceil( canvas.parentNode.clientWidth / 2 ) * 2;
			height = Math.ceil( canvas.parentNode.clientHeight / 2 ) * 2;
			tick = 0;

			lines.length = 0;
			canvas.width = width;
			canvas.height = height;
		}

		function create() {
			if( tick % 10 === 0 ) {
				lines.push( new line());
			}
		}

		function step() {
			var i = lines.length;
			while( i-- ) {
				lines[ i ].step( i );
			}
		}

		function clear() {
			ctx.globalCompositeOperation = 'destination-out';
			ctx.fillStyle = 'hsla(0, 0%, 0%, 0.1';
			ctx.fillRect( 0, 0, width, height );
			ctx.globalCompositeOperation = 'lighter';
		}

		function draw() {
			ctx.save();
			ctx.translate( width / 2, height / 2 );
			ctx.rotate( tick * 0.001 );
			var scale = 0.8 + Math.cos( tick * 0.02 ) * 0.2;
			ctx.scale( scale, scale );
			ctx.translate( -width / 2, -height / 2 );
			var i = lines.length;
			while( i-- ) {
				lines[ i ].draw( i );
			}
			ctx.restore();
		}

		function loop() {
			requestAnimationFrame( loop );
			create();
			step();
			clear();
			draw();
			tick++;
		}

		function onresize() {
			reset();
		}

		window.addEventListener( 'resize', onresize );

		init();

	};

	if(qs('#canva-grid')){
		canvasGrid('#canva-grid');
	}
	// ——————————————————————————————————————————————————
	// Canvas rainbow-grid animation /z
	// ——————————————————————————————————————————————————

}

z3w_Anim();


// if (Turbolinks.supported) {

// 	document.addEventListener('turbolinks:load', function(event) {
// 		z3w_Anim();
// 		alert('Turbolinks');
// 	});

// }