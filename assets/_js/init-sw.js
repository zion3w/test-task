'use strict';

/*************************************************************************
START
serviceWorker
************************************************************************/

// init
if ('serviceWorker' in navigator){
	navigator.serviceWorker.register('/sw.js')
}
// serviceWorker - add to homescreen
if ( ('serviceWorker' in navigator) && (bowser.blink) && (qs('.header-buttons')) ) {

	window.addEventListener('beforeinstallprompt', function(e) {
		e.preventDefault();
		var deferredPrompt;

		deferredPrompt = e;

		if(!qs('#a2hs')){
			var btnAdd = document.createElement('BUTTON');
			// var t = document.createTextNode('Добавить как приложение');
			// btnAdd.appendChild(t);
			btnAdd.setAttribute('id', 'a2hs');
			btnAdd.setAttribute('class', 'i_star');
			if (qs('html').getAttribute('lang')==='ru') {
				btnAdd.setAttribute('title', 'Установить как приложение');
			} else {
				btnAdd.setAttribute('title', 'Install as an application');
			}
			qs('.header-buttons').appendChild(btnAdd);
		}

		qs('#a2hs').addEventListener('click', function(e) {
			deferredPrompt.prompt();

			deferredPrompt.userChoice.then(function(choiceResult) {
					// if (choiceResult.outcome === 'accepted') {
					//  console.log('User accepted the A2HS prompt');
					// } else {
					//  console.log('User dismissed the A2HS prompt');
					// }
					deferredPrompt = null;
				});
		});

		window.addEventListener('appinstalled', function(evt) {
			qs('#a2hs').className += ' dn';
			console.log('pwa installed');
		});

	});

}
// serviceWorker - add to homescreen /z


/*************************************************************************
END
serviceWorker
************************************************************************/
