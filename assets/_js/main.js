'use strict';

/*************************************************************************
START
GLOBAL FUNCTIONS and VARIABLES
************************************************************************/


function qs(selector) {
	return document.querySelector(selector);
}
function qsa(selectorAll) {
	return document.querySelectorAll(selectorAll);
}



function onSubmit() {
	console.log('onSubmit');
	// grecaptcha.execute();
}



/*
global variables
*/
// get gtag for analytics
if (qs('head #gtag')) {
	var gtagID = qs('head #gtag').getAttribute('src').split('id=')[1];
}
window.dataLayer = window.dataLayer || [];
function gtag(){
	dataLayer.push(arguments);
}
// get gtag for analytics /z

var formCapchaWidget_V3;
var gRecapchaKey_v3;
var formCapchaWidget_V2;
var gRecapchaKey_v2;

// var grecaptcha_v3OnLoad = function() {
// 	widgetId = grecaptcha.render('componentId', {
// 		'sitekey' : 'yourKey',
// 		'callback' : onSubmit,
// 	});
// };

// FB Pixel
if (qs('head [data-fbpix]')) {
	var fbpix = qs('head [data-fbpix]').getAttribute('data-fbpix');
}


/* tooltipster */
var jtt_trigger = 'hover';
if(!qs('html.no-touchevents')){
	jtt_trigger = 'click';
}
/* tooltipster /z*/

var bLazy = new Blazy({
	selector: '.lz',
	src : 'data-lz',
	offset: 1200,
	// loadInvisible: true, //Set to true if you want to load invisible (hidden) elements.
	separator: '|', //Used if you want to pass retina images: data-src=”image.jpg|image@2x.jpg”
	successClass: 'll',
	saveViewportOffsetDelay: 100, //Delay for how often it should call the saveViewportOffset function on resize. Default is 50ms.
	validateDelay: 50, //Delay for how often it should call the validate function on scroll/resize. Default is 25ms.

	breakpoints: [{
		width: 450, // Max-width
		src: 'data-lz-450'
	},
	{
		width: 1400, // Max-width
		src: 'data-lz-1400'
	},
	{
		width: 1600, // Max-width
		src: 'data-lz-1600'
	}]
});

/* headroom.js */
// grab an element
// var headerElement = document.querySelector("body");
// construct an instance of Headroom, passing the element
var headroom = new Headroom(document.documentElement);
// initialise
headroom.init();
// if( typeof(headroom) !== 'undefined' ){
//  headroom.destroy();
// }
/* headroom.js END */


/*
global variables /z
*/

/*************************************** отложенная загрузка скриптов и стилей */
var jscssfileadded = '-';
function loadjscssfile(filename, filetype, attr){
	if (jscssfileadded.indexOf(filename) === -1){
		var fileref;
		if (filetype === 'js'){
			fileref = document.createElement('script');
			fileref.setAttribute('src', filename);
			if (attr) {
				fileref.setAttribute(attr, '');
			}
		}
		else if (filetype === 'css'){
			fileref = document.createElement('link');
			fileref.setAttribute('rel', 'stylesheet');
			fileref.setAttribute('type', 'text/css');
			fileref.setAttribute('href', filename);
		}
		if (typeof fileref !== 'undefined'){
			document.getElementsByTagName('head')[0].appendChild(fileref);
		}
		jscssfileadded += filename;
	}
}

function loadScript(src, callback) {
	var script = document.createElement('script');
	script.src = src;

	// script.onload = () => callback(null, script);
	script.onload = function(){
		return callback(null, script);
	}
	// script.onerror = () => callback(new Error(`Не удалось загрузить скрипт ${src}`));
	script.onerror = function(){
		return callback(new Error('Fail to load script: '+src));
	}

	document.head.append(script);
	jscssfileadded += src;
}
// loadScript('/my/script.js', function(error, script) {
//   if (error) {
//     // обрабатываем ошибку
//   } else {
//     // скрипт успешно загружен
//   }
// });
/*************************************** отложенная загрузка скриптов и стилей END*/


/* добавить-убрать класс для любого id */
function changeCL(clTOadd, selectorToChangeClass) {
	var body = qs(selectorToChangeClass);
	var regex = new RegExp('\\b' + clTOadd + '\\b');
	if (body.className.search(regex) !== -1) {
		body.className = body.className.replace(new RegExp('(?:^|\\s)' + clTOadd + '(?:\\s|$)'), ' ');
	} else {
		body.className += ' ' + clTOadd;
	}
}
/* добавить-убрать класс для любого id END*/

/* добавить класс для любого id */
function addCL(clTOadd, idToChangeClass) {
	var body = qs(idToChangeClass);
	var regex = new RegExp('\\b' + clTOadd + '\\b');
	if (body.className.search(regex) == -1) {
		body.className += ' ' + clTOadd;
	}
}

function adCl(elToChangeClass, elClassName) {
	if (typeof(elToChangeClass) === 'string') {
		elToChangeClass = qs(elToChangeClass);
	}
	var regex = new RegExp('\\b' + elClassName + '\\b');
	if (elToChangeClass.className.search(regex) == -1) {
		elToChangeClass.className += ' ' + elClassName;
	}
}
/* добавить класс для любого id  END*/

/* убрать класс для любого id */
function removeCL(clTOadd, idToChangeClass) {
	var body = qs(idToChangeClass);
	var regex = new RegExp('\\b' + clTOadd + '\\b');
	if (body.className.search(regex) !== -1) {
		body.className = body.className.replace(new RegExp('(?:^|\\s)' + clTOadd + '(?:\\s|$)'), ' ');
	}
}

function rmCl(elToChangeClass, elClassName) {
	if (typeof(elToChangeClass) === 'string') {
		elToChangeClass = qs(elToChangeClass);
	}
	var regex = new RegExp('(\\b|\\s)' + elClassName + '\\b');
	if (elToChangeClass.className.search(regex) !== -1) {
		elToChangeClass.className = elToChangeClass.className.replace(regex, '');
		// elToChangeClass.className = elToChangeClass.className.replace(elClassName,'');
	}
}
/* убрать класс для любого id  END*/

function hideClick(){
	this.className = 'dn';
}

/**************** To top */
function toTopFunction(){
	window.scrollTo(0,0);
}
/**************** To top END */

/* update menu state */
function updateMenu(menu_links){
	var current_url = window.location.pathname;
	var menu_a = qsa(menu_links);

	for(var i = menu_a.length-1; i >= 0; i--){
		if (menu_a[i].parentElement.className.indexOf('active') !== -1) {
			menu_a[i].parentElement.className = menu_a[i].parentElement.className.replace(/\bactive\b/g, '');
		}
	}

	var stop;
	for(var i = menu_a.length-1; i >= 0; i--){
		if ( current_url === menu_a[i].getAttribute('href') && i !== 0 ) {
			menu_a[i].parentElement.className += ' active';
			if (menu_a[i].parentElement.parentElement.className.indexOf('hide_ul') !== -1) {
				menu_a[i].parentElement.parentElement.parentElement.className += ' active';
			}
			stop = 1;
			break;
		} else if ( current_url === menu_a[i].getAttribute('href') && i == 0) {
			menu_a[i].parentElement.className += ' active';
		}
	}

	if (stop !== 1) {
		for(var i = menu_a.length-1; i >= 0; i--){
			if ( current_url.indexOf(menu_a[i].getAttribute('href')) !== -1 && i !== 0 ) {
				menu_a[i].parentElement.className += ' active';
				if (menu_a[i].parentElement.parentElement.className.indexOf('hide_ul') !== -1) {
					menu_a[i].parentElement.parentElement.parentElement.className += ' active';
				}
				break;
			} else if ( current_url === menu_a[i].getAttribute('href') && i == 0) {
				menu_a[i].parentElement.className += ' active';
			}
		}
	}
}
function updateSideMenu(menu_links){
	// var site_url = window.location.protocol+'//'+window.location.host+'/';
	// var current_url = event.data.url;
	var current_url = window.location.pathname;
	var menu_a = qsa(menu_links);

	for(var i = menu_a.length-1; i >= 0; i--){
		if (menu_a[i].parentElement.className.indexOf('active') !== -1) {
			menu_a[i].parentElement.className = menu_a[i].parentElement.className.replace(/\bactive\b/g, '');
		}
	}

	for(var i = menu_a.length-1; i >= 0; i--){
		if (current_url === menu_a[i].getAttribute('href')) {
			menu_a[i].parentElement.className += ' active';
			if (menu_a[i].parentElement.parentElement.className.indexOf('hide_ul') !== -1) {
				menu_a[i].parentElement.parentElement.parentElement.className += ' active';
			}
		}
	}
}
/* update menu state /z*/



/******************************************* Мобильное меню */
function mobileMenu() {
	if(!qs('body.mmo')){
		addCL('mmo', 'body');
		qs('main').addEventListener('click', closeMobileMenu);
	} else{
		closeMobileMenu();
	}
}
function closeMobileMenu() {
	removeCL('mmo', 'body');
	qs('main').removeEventListener('click', closeMobileMenu);
}
/******************************************* Мобильное меню END */



/**************** fullscreenmode for all page */
function toggleFullScreen() {
	if (!document.fullscreenElement &&    // alternative standard method
		!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
		if (document.documentElement.requestFullscreen) {
			document.documentElement.requestFullscreen();
		} else if (document.documentElement.msRequestFullscreen) {
			document.documentElement.msRequestFullscreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullscreen) {
			document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}
		qs('#fullscreen-mode').className = 'i_fw_';
	} else {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
		qs('#fullscreen-mode').className = 'i_fw';
	}
}
/**************** fullscreenmode for all page END */
/**************** fullscreenmode for images */
function toggleElemFullScreen() {
	if ( !document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {
		if (this.requestFullscreen) {
			this.requestFullscreen();
		} else if (this.msRequestFullscreen) {
			this.msRequestFullscreen();
		} else if (this.mozRequestFullScreen) {
			this.mozRequestFullScreen();
		} else if (this.webkitRequestFullscreen) {
			this.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	} else {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}
}
/**************** fullscreenmode for images END */
/**************** fullscreenmode for element */
function toggleFullScreenFor(forEl) {

	// var forEl = qs(el);
	// var forEl = el;

	if ( !document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {
		if (forEl.requestFullscreen) {
			forEl.requestFullscreen();
		} else if (forEl.msRequestFullscreen) {
			forEl.msRequestFullscreen();
		} else if (forEl.mozRequestFullScreen) {
			forEl.mozRequestFullScreen();
		} else if (forEl.webkitRequestFullscreen) {
			forEl.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	} else {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}
}
/**************** fullscreenmode for element /z*/

/* simple css tooltips */
function ttTooltips(){
	if (!this.getAttribute('data-tt')){
		var tt_title = this.title;
		this.setAttribute('data-tt', tt_title);
		this.title = '';
		this.removeEventListener('mouseover', ttTooltips);
	}
}
/* simple css tooltips /z*/

/***************************** ajax modal window with or without callback*/
function loadDoc(url, ajax_container_id, callbackFunc) {

	if(!qs('#z3_modal #'+ajax_container_id+'-id')){

		var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
		xhttp.open('GET', url, true);
		xhttp.onreadystatechange = function() {
			if (this.readyState === 4 && this.status === 200) {

				var ajax_response = xhttp.responseText;
				qs('#z3_modal').innerHTML = '<div id="'+ajax_container_id+'-id">' + ajax_response + '</div>';

				if (callbackFunc) {
					callbackFunc(this);
				}

				addCL('z3_modal','body');
			}
		};
		xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhttp.send();
	}  else {
		changeCL('z3_modal','body');
	}
}
/***************************** ajax modal window with or without callback*/



/**************** отправка почты форма заказа */

/****** форматирование инпута телефонного номера */
function phoneFormat(){
	var phone_number = this.value;
	var pattern = /(?!^\+{1})\D/g;
	this.value = phone_number.replace(pattern, '');
}
/****** форматирование инпута телефонного номера /zzz*/
/****** форматирование email инпута */
function emailFormat(){
	var phone_number = this.value;
	var pattern = /([`!#$%^&*=+\(\{\[\]\}\)\'\"\:\;\<\>\?\s/\\])/g;
	this.value = phone_number.replace(pattern, '');
}
/****** форматирование email инпута /zzz*/

// Cookies.set('check', 'form_id');
// Cookies.remove('check');
// Cookies.set('check', form_id);

function formAjax(){

	var url = this.getAttribute('data-form-url');
	var modalContainer = this.getAttribute('id')+ '-modal';

	if(!qs('#z3_modal #' + modalContainer)){

		var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
		xhttp.open('GET', url, true);
		xhttp.onreadystatechange = function() {
			if (this.readyState === 4 && this.status === 200) {

				qs('#z3_modal').innerHTML = '<div id="'+modalContainer+'" class="form_container">' + xhttp.responseText + '</div>';
				addCL('z3_modal','body');

				/* set focus on empty field */
				var formEl = qs('#'+modalContainer+' form');
				var formInputs = formEl.querySelectorAll('input');
				setTimeout( function() {
					for (var i = 0, arL=formInputs.length; i < arL; i++) {
						if(!formInputs[i].value){
							formInputs[i].focus();
							break;
						}
					}
				}, 650);
				/* set focus on empty field /z*/
				formInit(formEl);
				// formEl.addEventListener('submit', sendForm);

				// if(formEl.querySelector('textarea')){
				// 	autosize(formEl.querySelectorAll('textarea'));
				// }
				// if(typeof XRegExp === 'undefined'){
				// 	loadjscssfile('/a/xregexp-all.js', 'js');
				// }
			}
		};
		xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhttp.send();
	} else {
		changeCL('z3_modal','body');
		/* set focus on empty field */
		var formInputs = qsa('#'+modalContainer+' form input');
		setTimeout( function() {
			for (var i = 0, arL=formInputs.length; i < arL; i++) {
				if(!formInputs[i].value){
					formInputs[i].focus();
					break;
				}
			}
		}, 650);
		/* set focus on empty field /z*/
	}
}

function formInit(formEl) {

	if (typeof(formEl) !== 'object') {
		formEl = qs(formEl);
	}

	formEl.addEventListener('submit', formSend);

	var formElContainer = formEl.parentElement;


	if(formEl.querySelector('textarea')){
		autosize(formEl.querySelectorAll('textarea'));
	}
	if(typeof XRegExp === 'undefined'){
		loadjscssfile('/a/xregexp-all.js', 'js');
	}


	if (formElContainer.querySelector('#grecapcha-v3')) {

		gRecapchaKey_v3 = formElContainer.querySelector('#grecapcha-v3').getAttribute('data-grecapcha-v3-sitekey');

		loadScript(
			// 'https://www.google.com/recaptcha/api.js?render=explicit',
			// 'https://www.google.com/recaptcha/api.js?onload=onLoad&render=explicit',

			// 'https://www.google.com/recaptcha/api.js?render='+gRecapchaKey_v3,
			'https://www.google.com/recaptcha/api.js',
			function(error, script) {
				if (error) {
					console.log('ошибкa recaptcha/api.js: '+error);
				} else {

					// @todo: https://product.hubspot.com/blog/quick-guide-invisible-recaptcha

					grecaptcha.ready(function () {

						// https://developers.google.com/recaptcha/docs/display?authuser=1#render_param

						formCapchaWidget_V3 = grecaptcha.render('grecapcha-v3', {
							'sitekey': gRecapchaKey_v3,
							'badge': 'inline',
							'theme' : 'dark'
						})


						var grecaptchaAction = (formEl.id).replace(/[^A-Za-z]/g, '');
						grecaptcha.execute(formCapchaWidget_V3, { action: grecaptchaAction }).then(function (token) {

							var grecapchaInputV3 = document.createElement('INPUT');
							grecapchaInputV3.setAttribute('type', 'hidden');
							grecapchaInputV3.setAttribute('id', 'grecapcha-v3-response');
							grecapchaInputV3.setAttribute('name', 'grecapcha-v3-response');
							grecapchaInputV3.value = token;
							formEl.appendChild(grecapchaInputV3);

							// qs('#grecapcha-v3-response').value = token;
						});


						// var grecaptchaAction = (formEl.id).replace(/[^A-Za-z]/g, '');
						// grecaptcha.execute(gRecapchaKey_v3, { action: grecaptchaAction })
						// .then(function (token) {
						// 	qs('#grecapcha-v3-response').value = token;
						// });

					});

				}
			});
	}

}


function jformInit(formEl) {

	if (typeof(formEl) !== 'object') {
		formEl = qs(formEl);
	}
	// console.log(formEl);
	formEl.addEventListener('submit', jformSend);

	if(formEl.querySelector('textarea')){
		autosize(formEl.querySelectorAll('textarea'));
	}
	if(typeof XRegExp === 'undefined'){
		loadjscssfile('/a/xregexp-all.js', 'js');
	}
}


function formValidate(objForm) {

	var formFields = objForm.querySelectorAll('[name]');
	/* check required fields */
	for( var i = 0, l = formFields.length; i < l; i++ ){
		if( (formFields[i].hasAttribute('data-required') || formFields[i].hasAttribute('required')) && !formFields[i].value){
			if (!formFields[i].hasAttribute('data-error')) {
				formFields[i].setAttribute('data-error', i);
				formFields[i].parentElement.className += ' error';
			}
		} else if( formFields[i].type === 'email' && !emailXRegExp.test(formFields[i].value) && (formFields[i].hasAttribute('data-required') || formFields[i].hasAttribute('required')) ) {
			if (!formFields[i].hasAttribute('data-error')) {
				formFields[i].setAttribute('data-error', i);
				formFields[i].parentElement.className += ' error';
			}
		} else if( formFields[i].type === 'tel' && !telXRegExp.test(formFields[i].value) && (formFields[i].hasAttribute('data-required') || formFields[i].hasAttribute('required')) ) {
			if (!formFields[i].hasAttribute('data-error')) {
				formFields[i].setAttribute('data-error', i);
				formFields[i].parentElement.className += ' error';
			}
		} else if( formFields[i].tagName.toLowerCase() === 'textarea' && formFields[i].value.length < 15 && (formFields[i].hasAttribute('data-required') || formFields[i].hasAttribute('required')) ) {
			if (!formFields[i].hasAttribute('data-error')) {
				formFields[i].setAttribute('data-error', i);
				formFields[i].parentElement.className += ' error';
			}
		} else {
			formFields[i].removeAttribute('data-error');
			formFields[i].parentElement.className = formFields[i].parentElement.className.replace(' error','');
		}
	}
	if(objForm.querySelectorAll('.error')[0]){
		objForm.querySelectorAll('.error [name]')[0].focus();
		return false;
	}
	/* check required fields /z*/

	return true;

}


function jformSend(e) {

	var valitate = formValidate(this);
	if (!valitate) {
		e.preventDefault();
		return false;
	}
}


function formSend(e){
	e.preventDefault();

	var valitate = formValidate(this);

	if (!valitate) {
		return false;
	}

	var formFields = this.querySelectorAll('[name]');
	// /* check required fields */
	// for( var i = 0, l = formFields.length; i < l; i++ ){
	// 	if( formFields[i].hasAttribute('data-required') && !formFields[i].value){
	// 		if (!formFields[i].hasAttribute('data-error')) {
	// 			formFields[i].setAttribute('data-error', i);
	// 			formFields[i].parentElement.className += ' error';
	// 		}
	// 	} else if( formFields[i].type === 'email' && !emailXRegExp.test(formFields[i].value) && formFields[i].hasAttribute('data-required') ) {
	// 		if (!formFields[i].hasAttribute('data-error')) {
	// 			formFields[i].setAttribute('data-error', i);
	// 			formFields[i].parentElement.className += ' error';
	// 		}
	// 	} else if( formFields[i].type === 'tel' && !telXRegExp.test(formFields[i].value) && formFields[i].hasAttribute('data-required') ) {
	// 		if (!formFields[i].hasAttribute('data-error')) {
	// 			formFields[i].setAttribute('data-error', i);
	// 			formFields[i].parentElement.className += ' error';
	// 		}
	// 	} else if( formFields[i].tagName.toLowerCase() === 'textarea' && formFields[i].value.length < 15 && formFields[i].hasAttribute('data-required') ) {
	// 		if (!formFields[i].hasAttribute('data-error')) {
	// 			formFields[i].setAttribute('data-error', i);
	// 			formFields[i].parentElement.className += ' error';
	// 		}
	// 	} else {
	// 		formFields[i].removeAttribute('data-error');
	// 		formFields[i].parentElement.className = formFields[i].parentElement.className.replace(' error','');
	// 	}
	// }
	// if(this.querySelectorAll('.error')[0]){
	// 	this.querySelectorAll('.error [name]')[0].focus();
	// 	return false;
	// }
	// /* check required fields /z*/

	this.querySelector('[type="submit"]').disabled = true;

	var formEl = this;
	var formContainer = formEl.parentElement;
	var modalFormContainer = formContainer.parentElement;


	if (this.className.indexOf('form_inline') !== -1) {
		modalFormContainer.style.height = modalFormContainer.clientHeight+'px';
		modalFormContainer.className += ' form_container_inline form_sending';
	} else {
		modalFormContainer.className += ' form_sending';
	}


	var formData = new FormData();
	if (this.parentElement.querySelector('.form_header')) {
		formData.append('form_header', this.parentElement.querySelector('.form_header').innerText);
	}
	for( var i = 0, l = formFields.length; i < l; i++ ){
		formData.append(formFields[i].name, formFields[i].value);
	}


	if (this.id) {
		formData.append('formid', this.id);
	}
	formData.append('location_href', location.href);
	formData.append('document_referrer', document.referrer);
	// formData.append('prev_page', Cookies.get('prev_page'));
	formData.append('user_date', new Date());
	formData.append('user_info', 'lang: '+window.navigator.language+'; userAgent: '+window.navigator.userAgent+'; screen: '+screen.width+' x '+screen.height+'; history.length: '+history.length);


	var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	xhttp.open('POST', this.getAttribute('action'));
	xhttp.onreadystatechange = function () {
		if (xhttp.readyState === 4 && xhttp.status === 200) {

			if (xhttp.responseText.indexOf('send_ok') !== -1) {
				modalFormContainer.innerHTML = '<h1 class="form_send-ok green i_ok"></h1>';
				modalFormContainer.className = modalFormContainer.className.replace(' form_sending',' form_send');
				setTimeout( function() {
					rmCl('body', 'z3_modal');
				}, 5000);
				if (xhttp.responseText.indexOf('error') !== -1) {
					// if one of sendTo.php methods does not work in send.php
					console.log(xhttp.responseText);
				}

			} else if(xhttp.responseText.indexOf('send_spam') !== -1){
				// if spam
				modalFormContainer.innerHTML = '<h1 class="form_send-error red i_hand"></h1><p>Too many messages!</p>';
				modalFormContainer.className = modalFormContainer.className.replace(' form_sending',' form_send');
				console.log(xhttp.responseText);

			} else if( formContainer.querySelector('#grecapcha-v3') && xhttp.responseText.indexOf('recaptcha_response_score') !== -1){

				// google recapcha v3

				var grecaptchaAction = (formContainer.querySelector('form').id).replace(/[^A-Za-z]/g, '');
				grecaptcha.execute(formCapchaWidget_V3, { action: grecaptchaAction }).then(function (token) {
					formContainer.querySelector('#grecapcha-v3-response').value = token;
				});


				// if spammer
				// formContainer.innerHTML = '<h1 class="form_send-error red i_hand"></h1><p>Google thinks you\'re a spammer!<br>Try to write directly to our email</p>';

				modalFormContainer.classList.remove('form_sending');
				modalFormContainer.classList.add('recaptcha_score');


				gRecapchaKey_v2 = formContainer.querySelector('#grecapcha-v2-invisible').getAttribute('data-grecapcha-v2-sitekey');

				if (!formContainer.querySelector('#grecapcha-v2-invisible .grecaptcha-badge')) {

					formCapchaWidget_V2 = grecaptcha.render('grecapcha-v2-invisible', {
						'sitekey': gRecapchaKey_v2,
						'badge': 'inline',
						'callback' : function(tokenV2){
							// @info: strange, but: tokenV2 === tokenV3

							// console.log('formCapchaWidget_V2 callback');


							var grecapchaInputV2 = document.createElement('INPUT');
							grecapchaInputV2.setAttribute('type', 'hidden');
							grecapchaInputV2.setAttribute('id', 'grecapcha-v2-response');
							grecapchaInputV2.setAttribute('name', 'grecapcha-v2-response');
							grecapchaInputV2.value = tokenV2;
							formEl.appendChild(grecapchaInputV2);


							modalFormContainer.classList.remove('recaptcha_score');
							modalFormContainer.classList.add('form_sending');

							formEl.querySelector('[type="submit"]').click();

						}
					});

				}

				// call capcha v2 if capcha v3 failed
				grecaptcha.execute(formCapchaWidget_V2);

				formEl.querySelector('[type="submit"]').disabled = false;

				return;
				// google recapcha v3/
			} else {
				// if none of sendTo.php methods does not works in send.php
				formContainer.innerHTML = '<h1 class="form_send-error red i_search_code"></h1><p>Server Error</p>';
				formContainer.className = formContainer.className.replace(' form_sending',' form_send');
				console.log(xhttp.responseText);
			}

		} else if(xhttp.readyState === 4 && xhttp.status !== 200){
			// if error in send.php script
			formContainer.innerHTML = '<h1 class="form_send-error red i_search_code"></h1><p>Server Error 404</p>';
			formContainer.className = formContainer.className.replace(' form_sending',' form_send');
			console.log(xhttp.responseText);
		}
	};
	xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhttp.send(formData);

}


/****** infinite waypoint scrolling */

function infiniteScroll( v_element, v_items, v_more ) {
	if (!qs(v_more)) {
		return;
	}
	var nextLink = qs(v_more).getAttribute('href');
	if (nextLink.indexOf('tmpl=component') === -1 && nextLink.indexOf('tmpl=ajax-list') === -1) {
		if (nextLink.indexOf('?') === -1) {
			qs(v_more).setAttribute('href', nextLink + '?tmpl=component');
		} else {
			qs(v_more).setAttribute('href', nextLink + '&tmpl=component');
		}
	}
	var infinite = new Waypoint.Infinite({
		element: v_element,
		items: v_items,
		offset: function() {
			return this.context.innerHeight() - $(v_element).outerHeight() + 1000;
		},
		more: v_more,
		onAfterPageLoad: function() {
			setTimeout(function() {
				bLazy.revalidate();
			}, 5);

			$('.jtt:not(.tooltipstered)').tooltipster({
				contentAsHTML: true,
				interactive: true,
				arrow: false,
				zIndex: 9,
				repositionOnScroll: true,
				maxWidth: 750,
				debug: false,
				restoration: 'current',
				trigger: jtt_trigger
			});
		}
	});

}
//infiniteScroll( v_element, v_items, v_more )
/****** infinite waypoint scrolling */

/*************************************************************************
END
GLOBAL FUNCTIONS and VARIABLES
************************************************************************/



/*************************************************************************
START
FUNCTION CALLS
************************************************************************/


/**************
start
define main global function
**************/

/*
MainScriptsOnload
*/
function MainScriptsOnload() {

	/* фикс подсветки родителя алиас-меню */
	/*
	то что есть(for x2-seek!) нужно упростить - if qs('.hide_ul .active').взять родителя и первой ссылке в нем дать класс актив тоже
	смотри функцию, что с турболинками работает
	*/

	/*
	var hidden_ul = document.getElementsByClassName('hide_ul');
	Loop1:
	for (var i = 0, a=hidden_ul.length; i < a; i++) {
		var child_menu = hidden_ul[i].childNodes;
		var parent_menu = hidden_ul[i].parentNode;
		for (var c = 0, b=child_menu.length; c < b; c++) {
			if ((child_menu[c].className.indexOf('active') !== -1) && (parent_menu.className.indexOf('active') == -1)) {
				parent_menu.className += ' active_alias';
				break Loop1;
			}
		}
	}
	*/
	/* фикс подсветки родителя алиас-меню END*/

	/********* tooltipster */
	if (qs('.jtt')) {
		$('.jtt').tooltipster({
			contentAsHTML: true,
			// contentCloning: true,
			interactive: true,
			arrow: false,
			zIndex: 9,
			repositionOnScroll: true,
			maxWidth: 750,
			debug: false,
			restoration: 'current',
			trigger: jtt_trigger
		});
	}
	/********* tooltipster /z*/

	if(qs('a[data-scroll]') || qs('article a[href^="#"]')){
		var header_height = qs('#header').offsetHeight;
		$('a[data-scroll]').smoothScroll({
			offset: -header_height,
			speed: 350,
			preventDefault: false
		});
		$('article a[href^="#"]').smoothScroll({
			offset: -header_height,
			speed: 350,
			preventDefault: true,
			beforeScroll: function(options) {
				if(history.pushState) {
					history.pushState(null, null, options.scrollTarget);
				} else {
					location.hash = options.scrollTarget;
				}
			},
		});
	}

	/************************** system-message-container */
	if (qs('#system-message-container')) {
		qs('#system-message-container').addEventListener('click', hideClick);
	}
	/************************** system-message-container /z*/

	/**************** замена title на data-tt в подсказках .tt*/
	if(qs('.tt')){
		var ttTitles = qsa('.tt');
		for (var i_tt = 0, ttL = ttTitles.length; i_tt < ttL; i_tt++) {
			ttTitles[i_tt].addEventListener('mouseover', ttTooltips);
		}
	}
	/**************** замена title на data-tt в подсказках .tt END*/



	/* accordion */
	if (qs('[data-accordion-btn]')) {
		var accBtn = qsa('[data-accordion-btn]');
		var regex = new RegExp('(\\b|\\s)active\\b');
		var accContent;
		for(var i = 0, l = accBtn.length; i < l; i++){
			accBtn[i].addEventListener('click', function(){
				accContent = qs('[data-accordion-content="'+ this.getAttribute('data-accordion-btn') + '"]');
				if (accContent.className.search(regex) !== -1) {
					rmCl(this, 'active');
					rmCl(accContent, 'active');
					accContent.style.height = '';
				} else {
					adCl(this, 'active');
					accContent.style.height = accContent.scrollHeight + 'px';
					adCl(accContent, 'active');
				}
			});
		}
	}
	/* accordion /z*/



	/*************** сменить дизайн блога*/
	if(qs('.blg_dsgn-btn')){

		qs('.blg_dsgn-btn').addEventListener('click', (function () {
			if(!Cookies.get('blg_dsgn')){
				Cookies.set('blg_dsgn', 'only_links', {expires: 365});
				document.location.reload(true);
			} else {
				Cookies.remove('blg_dsgn');
				document.location.reload(true);
			}
		}));

	}
	/*************** сменить дизайн блога /z*/


	/******************* кнопка погода и выбор погоды */

	if(qs('#pogoda')){
		qs('#pogoda').addEventListener('click', (function () {

			loadjscssfile('/a/pogoda.css', 'css');

			var pogoda_gorod_link;

			if(!Cookies.get('pg_link')){
				pogoda_gorod_link = 'default';
			} else {
				pogoda_gorod_link = Cookies.get('pg_link');
			}

			loadDoc(
				'/pogoda/'+pogoda_gorod_link+'.htm',
				'pogoda_container');

		}));

		qs('.pogoda i').addEventListener('click', (function(){

			loadDoc(
				'/pogoda/pogoda-mesta.html',
				'pogoda-mesta',
				pogodaVyborMesta);

			function pogodaVyborMesta () {
				var ul, li, i;
				ul = qs('#pogoda_mesta ul');
				li = ul.getElementsByTagName('li');
				i;

				for (i = 0; i < li.length; i++) {
					li[i].addEventListener('click', getMestoCookies);
				}

				function getMestoCookies(){
					var pg_link = this.getAttribute('data-pg-link');
					var pg_mesto = this.innerText;
					Cookies.set('pg_link', pg_link, {expires: 365});
					Cookies.set('pg_mesto', pg_mesto, {expires: 365});
					changeCL('z3_modal','body');
				}

				qs('#pg-mesta-search').addEventListener('keyup', function(){
					var filter = this.value.toUpperCase();
					for (i = 0; i < li.length; i++) {
						if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
							li[i].style.display = '';
						} else {
							li[i].style.display = 'none';
						}
					}
				});
			}

		}));

	}

	/******************* кнопка погода и выбор погоды /z*/

}
/*
MainScriptsOnload /z
*/


function articleVoting(event) {
	event.preventDefault();
	this.removeEventListener('click', articleVoting);

	var vote_num = this.getAttribute('data-vote');

	var formdata = new FormData();
	formdata.append('user_rating', vote_num);

	var form_id ='.content_vote';

	for (var i2 = 0, inL = qsa(form_id+' input[type="hidden"]').length; inL > i2; i2++ ) {
		formdata.append(qsa(form_id+' input[type="hidden"]')[i2].name, qsa(form_id+' input[type="hidden"]')[i2].value);
	}

	var post_url = qs(form_id+' [name="url"]').value;

	var ajax = new XMLHttpRequest();
	ajax.open('POST', post_url, true);

	ajax.onreadystatechange = function () {

		if (this.readyState == 4 && this.status == 200) {
			var htmlString = this.responseText,
			parser = new DOMParser(),
			doc = parser.parseFromString(htmlString, 'text/html');
			addCL('has_vote', '#art-vote');
			qs('#response-vote').innerHTML = doc.querySelector('#system-message-container .sys_message').innerText;
		}

	};
	ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	ajax.send(formdata);

}

/*
blog full article scripts
*/
function BlogArticlePage() {

	/* hide reapeated image in article*/
	if(qs('.article_image img')){
		var full_img_link = qs('.article_image img').getAttribute('src');
		var art_body_images = qsa('#articlebody img');
		for (var i = 0; i < art_body_images.length; i++) {
			if(art_body_images[i].getAttribute('src')===full_img_link){
				art_body_images[i].className = 'dn';
			}
		}
	}
	/* hide reapeated image in article /z*/

	/* set https on http iframes */
	if(qs('#articlebody iframe')){
		var iframes = qsa('#articlebody iframe');
		// var iframe_link = qs('iframe').getAttribute('src');
		var iframe_src;
		for (var i0 = 0; i0 < iframes.length; i0++) {
			iframe_src = iframes[i0].getAttribute('src');
			iframes[i0].setAttribute('allowfullscreen', '');
			if(iframe_src.substring(0,5)!=='https'){
				iframe_src = iframe_src.replace('http','https');
			}
			if(iframe_src.indexOf('?') !== -1){
				iframes[i0].setAttribute('src', iframe_src + '&rel=0');
			} else {
				iframes[i0].setAttribute('src', iframe_src + '?rel=0');
			}
		}
	}
	/* set https on http iframes /z*/

	/* for instagram embed posts */
	if(qs('blockquote[data-instgrm-captioned]')){
		var insta_posts = qsa('blockquote[data-instgrm-captioned]');
		for (var i = 0, a=insta_posts.length; i < a; i++) {
			insta_posts[i].className = 'instagram-media';
		}
		loadjscssfile('//www.instagram.com/embed.js', 'js');
	}
	/* for instagram embed posts /z*/

	if(qs('.full_article img')){
		var article_images = qsa('.full_article img');
		for (var i1 = 0, b1=article_images.length; i1 < b1; i1++) {
			article_images[i1].addEventListener('click', toggleElemFullScreen);
		}
	}

	/*ajax native joomla voting*/
	if (qs('.vote_star')) {
		var vote_star = qsa('.vote_star');

		for (var i = 0, b=vote_star.length; i < b; i++) {
			vote_star[i].addEventListener('click', articleVoting);
		}
	}
	/*ajax native joomla voting*/


	/* disqus comment system */
	//@todo: uncomment on production
	if (qs('#disqus_thread') && qs('head #prod')) {
		if( typeof(DISQUS) !== 'undefined' ){

			var disq_div=qs('#disqus_thread');

			DISQUS.reset({
				reload: true,
				config: function () {
					this.page.identifier = disq_div.getAttribute('data-art-id');
					this.page.url = disq_div.getAttribute('data-art-link');
				}
			});

		} else {
			setTimeout(function() {
				loadjscssfile('/a/disqus.js', 'js');
			}, 2500);
		}
	}
	/* disqus comment system /z*/

}
/*
blog full article scripts /z
*/


/* modal login */
function _modalLogin(e) {

	e.preventDefault();

	var loginButton = this;
	// var ajaxRequestUrl = this.getAttribute('data-ajax-url');
	var ajaxRequestUrl = '/ajax?tmpl=modal-login';

	var modalDivContainer = qs('#modal_container');
	var modalContainerInnerSelector = 'body > #modal_container > [data-modal-url="'+ajaxRequestUrl+'"]';

	var current_url_base64 = btoa(window.location.href);
	var current_url_full = window.location.href;


	if (!qs(modalContainerInnerSelector)) {

		var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
		xhttp.open('GET', ajaxRequestUrl, true);
		xhttp.onreadystatechange = function() {
			if (this.readyState === 4 && this.status === 200) {

				modalDivContainer.innerHTML += '<div class="modal_child modal_child-active" data-modal-url="'+ajaxRequestUrl+'">'+xhttp.responseText+'</div>';

				addCL('modal_container-visible','body');

				// loadjscssfile('//apis.google.com/js/platform.js', 'js');
				// loadjscssfile('//connect.facebook.net/'+qs('html').getAttribute('lang')+'/sdk.js', 'js');

				/* set focus on empty field */
				var formEl = qs(modalContainerInnerSelector+' form#module-login');
				var formInputs = formEl.querySelectorAll('input');

				setTimeout( function() {
					for (var i = 0, arL=formInputs.length; i < arL; i++) {
						if(!formInputs[i].value){
							formInputs[i].focus();
							break;
						}
					}

					loadjscssfile('/_js/a/socialnetworks-login.js', 'js');

					qs('#module-login').setAttribute('action', current_url_full);
					qs('#module-login-form-return').setAttribute('value', current_url_base64);

				}, 575);

			}
		};
		xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhttp.send();
	} else {
		changeCL('modal_container-visible','body');
		qs(modalContainerInnerSelector).className += ' modal_child-active';
		/* set focus on empty field */
		var formInputs = qsa(modalContainerInnerSelector+' form#module-login input');
		setTimeout( function() {
			for (var i = 0, arL=formInputs.length; i < arL; i++) {
				if(!formInputs[i].value){
					formInputs[i].focus();
					break;
				}
			}
			qs('#module-login').setAttribute('action', current_url_full);
			qs('#module-login-form-return').setAttribute('value', current_url_base64);
		}, 575);
	}


	// console.log(loginButton);

}
/* modal login /z*/

// function EkerlabsPowerContent(){

// 	if(qs('#blog-comments-fullscreen')){
// 		qs('#blog-comments-fullscreen').addEventListener('click',
// 			(function () {
// 				var formTag = this.parentElement;
// 				while (formTag.tagName !== 'FORM') {
// 					formTag = formTag.parentElement;
// 				}
// 				// formTag = formTag.getAttribute('id');
// 				// alert(formTag);
// 				// toggleFullScreenFor('#' + formTag);
// 				toggleFullScreenFor(formTag);
// 			}));
// 	}

// 	if (qs('.comments_form textarea')) {
// 		autosize(qs('.comments_form textarea'));
// 	}

// 	var EpcCommentsManager, EpcVotingManager, EpcLivePaginationManager;

// 	jQuery(EpcCommentsManager = function(){


// 		/********* tooltipster for comments or any html tooltip */
// 		if (qs('.jtt-html')) {
// 			$('.jtt-html').tooltipster({
// 				contentAsHTML: true,
// 				contentCloning: true,
// 				interactive: true,
// 				arrow: false,
// 				zIndex: 9,
// 				maxWidth: 750,
// 				debug: true,
// 				side: ['right', 'top', 'bottom', 'left'],
// 				restoration: 'current',
// 				plugins: ['sideTip', 'scrollableTip'],
// 				trigger: 'click',
// 				functionPosition: function(instance, helper, position){
// 					var ttl = qsa('.tooltipster-base').length;
// 					if(ttl > 1){
// 						position.coord.top = 10 + qsa('.tooltipster-base')[ttl-2].getBoundingClientRect().top;
// 						position.coord.left = 10 + qsa('.tooltipster-base')[ttl-2].getBoundingClientRect().left;
// 					}
// 					return position;
// 				},
// 			});
// 		}
// 		/********* tooltipster for comments or any html tooltip /z*/

// 		var that = this;

// 		this.init = function(){
// 			that.initCommentProcessor();
// 			that.allowReplies();
// 		};

// 		this.processor = function($activeForm){

// 			var $form            = $activeForm,
// 			$reqField            = $form.find('[data-required]'),
// 			$messagesContainer   = $form.find('[data-epc-comments-messages-container]'),
// 			messageClass         = 'epc-comments-message',
// 			$epcSpinner          = $form.find('[data-epc-spinner]'),
// 			$submitBtn           = $form.find('[data-epc-submit-comment]'),
// 			that                 = this;

// 			for( var i = 0, l = $reqField.length; i < l; i++ ){
// 				if( $reqField[i].hasAttribute('data-required') && !$reqField[i].value){
// 					if (!$reqField[i].parentElement.hasAttribute('data-error')) {
// 						$reqField[i].parentElement.setAttribute('data-error', i);
// 						$reqField[i].parentElement.className += ' error';
// 					}
// 				} else {
// 					$reqField[i].parentElement.removeAttribute('data-error');
// 					$reqField[i].parentElement.className = $reqField[i].parentElement.className.replace(' error','');
// 					// $reqField[i].parentElement.className = $reqField[i].parentElement.className.replace(/ error/g,'');
// 				}
// 			}

// 			for( i = 0, l = $reqField.length; i < l; i++ ){
// 				if($reqField[i].parentElement.className.indexOf('error') > -1){
// 					$reqField[i].focus();
// 					return false;
// 				}
// 			}

// 			this.init = function(){
// 				that.startLoader();
// 				that.voteAjax();
// 			};

// 			this.getDataForAjax = function(){

// 				var data = {
// 					commenter       : $form.find('[name="epc[commenter]"]').val(),
// 					comment         : $form.find('[name="epc[comment]"]').val(),
// 					email           : $form.find('[name="epc[email]"]').val(),
// 					url             : $form.find('[name="epc[url]"]').val(),
// 					profession      : $form.find('[name="epc[profession]"]').val(),
// 					itemId          : $form.find('[name="epc[itemId]"]').val(),
// 					parentId        : $form.find('[name="epc[parentId]"]').val(),
// 					malkodetDvash   : $form.find('[name="epc_malkodet_dvash"]').val()
// 				};

// 				if( typeof grecaptcha != 'undefined' ) {
// 					data.reCaptcha = grecaptcha.getResponse()
// 				}

// 				return data;

// 			};

// 			this.voteAjax = function(){

// 				var commentData = that.getDataForAjax();

// 				jQuery.ajax({
// 					type: 'POST',
// 					url: 'index.php?option=com_epc&view=item&task=item.addCommentAjax&format=raw',
// 					data: commentData,
// 					success: function (response) {
// 						setTimeout(function(){
// 							that.handleResponse(response);
// 						}, 500);
// 					},
// 					error: function (error) {
// 						console.log('AJAX error: ' + JSON.stringify(error, null, 2));
// 					}
// 				});

// 			};

// 			this.startLoader = function(){
// 				$epcSpinner.css('display', 'inline-block');
// 				$submitBtn.prop('disabled', true);
// 				$submitBtn.addClass('epc-processing');
// 			};

// 			this.endLoader = function(){
// 				$epcSpinner.css('display', 'none');
// 				$submitBtn.prop('disabled', false);
// 				$submitBtn.removeClass('epc-processing');
// 			};

// 			this.handleResponse = function(response){

// 				response = JSON.parse(response);

// 				switch (response.type) {

// 					case 'success':
// 					that.handleSuccess(response);
// 					break;

// 					case 'error':
// 					that.handleError(response);
// 					break;

// 					case 'pending':
// 					that.handlePending(response);
// 					break;

// 				}

// 			};

// 			this.handleSuccess = function(response){
// 				that.showMessage(response.type, response.message);
// 				location.reload()
// 			};

// 			this.handlePending = function(response){
// 				that.showMessage(response.type, response.message);
// 			};

// 			this.handleError = function(response){
// 				that.showMessage(response.type, response.message);
// 				that.refreshReCaptcha();
// 			};

// 			this.refreshReCaptcha = function(){
// 				if( typeof grecaptcha != 'undefined' ) {
// 					grecaptcha.reset();
// 				}
// 			};

// 			this.showMessage = function($type, $message){
// 				jQuery('.' + messageClass).remove();
// 				jQuery('<span class="'+messageClass + ' ' + $type+'">'+$message+'</span>').appendTo($messagesContainer);
// 				that.endLoader();
// 			};

// 			return this.init();

// 		};

// 		this.initCommentProcessor = function(){

// 			jQuery('[data-epc-comments-form ]').on('submit', function(e){
// 				e.preventDefault();
// 				that.processor(jQuery(this));
// 			});

// 		};

// 		this.allowReplies = function(){

// 			jQuery('[data-epc-comment-reply]').on('click', function(e){

// 				e.preventDefault();

// 				adCl(this, 'active');

// 				jQuery('.epc-reply-form').remove();

// 				var commentId   = jQuery(this).attr('data-epc-comment-reply-id'),
// 				formCopy    = jQuery('[data-epc-comments-form]').first().clone(),
// 				parentInput = '<input name="epc[parentId]" type="hidden" value="'+commentId+'">';

// 				formCopy.find('.g-recaptcha').remove();
// 				formCopy.addClass('epc-reply-form');
// 				formCopy.find('[data-epc-comments-messages-container]').empty();
// 				jQuery(parentInput).appendTo(formCopy);


// 				for(var i = 0, l = formCopy[0].length; i < l; i++){
// 					if (formCopy[0][i].hasAttribute('tabindex')) {
// 						formCopy[0][i].setAttribute('tabindex', '10'+[i]);
// 					}
// 					if (formCopy[0][i].hasAttribute('id')) {
// 						formCopy[0][i].setAttribute('id', formCopy[0][i].getAttribute('id')+'1');
// 					}
// 				}

// 				var formLabels = formCopy.find('label');
// 				for(var i = 0, l = formLabels.length; i < l; i++){
// 					formLabels[i].setAttribute('for', formLabels[i].getAttribute('for')+'1');
// 				}


// 				formCopy.insertAfter(jQuery(this));
// 				that.initCommentProcessor();

// 				autosize(qs('.epc-reply-form textarea'));

// 				qsa('.comments_form-fullscreen')[qsa('.comments_form-fullscreen').length -1].addEventListener('click',
// 					(function () {
// 						var formTag = this.parentElement;
// 						while (formTag.tagName !== 'FORM') {
// 							formTag = formTag.parentElement;
// 						}
// 						toggleFullScreenFor(formTag);
// 					}));

// 			});

// 		};

// 		return this.init();

// 	});

// jQuery(EpcVotingManager = function(){

// 	var epcVote              = 'data-epc-vote',
// 	$numberUp            = jQuery('[data-epc-current-vote="up"]'),
// 	$numberDown          = jQuery('[data-epc-current-vote="down"]'),
// 	$messagesContainer   = jQuery('[data-epc-votes-messages-container]'),
// 	$votingContainer     = jQuery('[data-epc-voting-container]'),
// 	messageClass         = 'epc-vote-message',
// 	$epcSpinner          = $votingContainer.find('[data-epc-spinner]'),
// 	itemId               = jQuery('[data-epc-vote-item-id]').attr('data-epc-vote-item-id'),
// 	that                 = this;

// 	this.init = function(){

// 		jQuery('['+epcVote+']').on('click', function(){
// 			that.startLoader();
// 			that.voteAjax('update', jQuery(this).attr(epcVote));
// 		});

// 		if( $votingContainer.length > 0 ) {
// 			that.voteAjax('load');
// 		}

// 	};

// 	this.buildDataForAjax = function(voteDirection){

// 		return {
// 			voteDirection : voteDirection,
// 			itemId        : itemId
// 		};

// 	};

// 	this.voteAjax = function(action, voteDirection){

// 		var voteData = that.buildDataForAjax(voteDirection),
// 		postUrl  = '';

// 		if( action === 'load' ) {
// 			postUrl = 'index.php?option=com_epc&view=item&task=item.getCurrentVotesAjax&format=raw';
// 		}

// 		if( action === 'update' ) {
// 			postUrl = 'index.php?option=com_epc&view=item&task=item.updateVoteAjax&format=raw';
// 		}

// 		jQuery.ajax({
// 			type: 'POST',
// 			url: postUrl,
// 			data: voteData,
// 			success: function (response) {
// 				that.handleResponse(response, voteDirection);
// 			},
// 			error: function (error) {
// 				console.log('AJAX error: ' + JSON.stringify(error, null, 2));
// 			}
// 		});

// 	};

// 	this.handleResponse = function(response, voteDirection){

// 		response = JSON.parse(response);

// 		if( response.type == 'success' ) {
// 			that.handleSuccess(voteDirection, response.message);
// 		}
// 		if( response.type == 'update' ) {
// 			that.handleUpdate(voteDirection, response.message);
// 		}
// 		if( response.type == 'error' ) {
// 			that.handleError(response.message);
// 		}
// 		if( response.type == 'load' ) {
// 			that.updateUiOnLoad(response);
// 		}

// 	};

// 	this.updateUiOnLoad = function(response){

// 		var votesUp   = parseInt($numberUp.text()),
// 		votesdown = parseInt($numberDown.text());

// 		if( votesUp !== parseInt(response.array['upVotes'])) {
// 			$numberUp.text(response.array['upVotes']);
// 		}
// 		if( votesdown !== parseInt(response.array['downVotes'])) {
// 			$numberDown.text(response.array['downVotes']);
// 		}

// 	};

// 	this.handleError = function(message){
// 		that.showMessage('error', message);
// 	};

// 	this.handleSuccess = function(voteDirection, message){

// 		if( voteDirection == 'up' ) {
// 			var currentUpNumber = parseInt($numberUp.text());
// 			$numberUp.text(currentUpNumber + 1);
// 		}
// 		if( voteDirection == 'down' ) {
// 			var currentDownNumber = parseInt($numberDown.text());
// 			$numberDown.text(currentDownNumber + 1);
// 		}

// 		that.showMessage('success', message);

// 	};

// 	this.handleUpdate = function(voteDirection, message){

// 		var currentUpNumber   = parseInt($numberUp.text()),
// 		currentDownNumber = parseInt($numberDown.text());

// 		if( voteDirection == 'up' ) {
// 			$numberUp.text(currentUpNumber + 1);

// 			if( currentDownNumber > 0 ) {
// 				$numberDown.text(currentDownNumber - 1);
// 			}

// 		}

// 		if( voteDirection == 'down' ) {
// 			$numberDown.text(currentDownNumber + 1);

// 			if( currentUpNumber > 0 ) {
// 				$numberUp.text(currentUpNumber - 1);
// 			}

// 		}

// 		that.showMessage('update', message);

// 	};

// 	this.showMessage = function(type, message){

// 		setTimeout(function(){
// 			jQuery('.' + messageClass).remove();
// 			jQuery('<span class="'+messageClass + ' ' + type+'">'+message+'</span>').appendTo($messagesContainer);
// 			that.endLoader();
// 		}, 500);

// 	};

// 	this.startLoader = function(){
// 		$votingContainer.addClass('epc-processing');
// 		$epcSpinner.addClass('epc-processing');
// 		$epcSpinner.css('display', 'inline-block')
// 	};

// 	this.endLoader = function(){
// 		$votingContainer.removeClass('epc-processing');
// 		$epcSpinner.removeClass('epc-processing');
// 		$epcSpinner.css('display', 'none')
// 	};

// 	return this.init();
// });

// jQuery(EpcLivePaginationManager = function(){

// 	var $loadMoreBtn   = jQuery('[data-epc-pagination-load-more]'),
// 	trigger        = $loadMoreBtn.attr('data-epc-pagination-load-more'),
// 	epcItem        = '[data-epc-item]',
// 	total          = $loadMoreBtn.attr('data-epc-pagination-total'),
// 	limitAttr      = 'data-epc-pagination-limit',
// 	idAttr         = 'data-epc-pagination-record-id',
// 	viewAttr       = 'data-epc-pagination-record-view',
// 	$spinner       = jQuery('[data-epc-spinner]'),
// 	that           = this;

// 	this.init = function(){

// 		if( trigger == 'click' ) {
// 			that.triggerByClick();
// 		}

// 		if( trigger == 'scroll' ) {
// 			$loadMoreBtn.css('display', 'none');
// 			that.triggerByScroll();
// 		}

// 	};

// 	this.triggerByClick = function(){

// 		$loadMoreBtn.on('click', function(){

// 			var data = that.getDataForAjax();

// 			that.startSpinner();
// 			that.triggerLoadMore(data.limit, data.offset, data.id, data.view);

// 		});

// 	};

// 	this.triggerByScroll = function(){

// 		var triggerDepth  = that.calculateExecution(),
// 		scrollLock    = false,
// 		throttle      = false;

// 		jQuery(window).scroll(function () {

// 			if( scrollLock == false ) {

// 				if (throttle == false) {

// 					var scrollPosition = jQuery(window).scrollTop();

// 					if (scrollPosition >= triggerDepth) {

// 						throttle = true;

// 						var data = that.getDataForAjax(),
// 						success = that.triggerLoadMore(data.limit, data.offset, data.id, data.view);

// 						if (success === true) {

// 							setTimeout(function () {

// 								triggerDepth = that.calculateExecution();
// 								throttle = false;

// 								if (parseInt(total) == parseInt(jQuery(epcItem).length)) {
// 									scrollLock = true;
// 								}

// 							}, 1500);

// 						}

// 					}

// 				}

// 			}
// 		});


// 	};

// 	this.calculateExecution = function(){

// 		var lastItemHeight     = jQuery(epcItem).last().height(),
// 		lastItemOffsetTop  = jQuery(epcItem).last().offset().top,
// 		triggerOffset      = lastItemHeight + lastItemOffsetTop;

// 		return Math.round(triggerOffset - jQuery(window).height() - 100);

// 	};

// 	this.getDataForAjax = function(){

// 		return {
// 			limit   :   $loadMoreBtn.attr(limitAttr),
// 			offset  :   jQuery(epcItem).length,
// 			id      :   $loadMoreBtn.attr(idAttr),
// 			view    :   $loadMoreBtn.attr(viewAttr)
// 		};

// 	};

// 	this.triggerLoadMore = function(limit, offset, id, view){

// 		var postUrl = 'index.php?option=com_epc&view=item&task=item.livePaginationAjax&format=raw';

// 		var data = {
// 			limit   : limit,
// 			offset  : offset,
// 			id      : id,
// 			view    : view
// 		};

// 		jQuery.ajax({
// 			type: 'POST',
// 			url: postUrl,
// 			data: data,
// 			success: function (response) {
// 				that.handleAjaxResponse(response);

// 				setTimeout(function() {
// 					bLazy.revalidate();
// 				}, 5);
// 			},
// 			error: function (error) {
// 				console.log('AJAX error: ' + JSON.stringify(error, null, 2));
// 			}
// 		});

// 		return true;

// 	};

// 	this.handleAjaxResponse = function(response){

// 		response = JSON.parse(response);

// 		if( response.type == 'error' ) {
// 			jQuery('.epc-load-more-error').remove();
// 			jQuery('<span class="epc-load-more-error error">'+response.message+'</span>').insertBefore($loadMoreBtn);
// 			that.removeSpinner();
// 			return false;
// 		}

// 		if( response.type == 'success' ) {

// 			jQuery(response.message).insertAfter(jQuery(epcItem).last());

// 			var items = parseInt(jQuery(epcItem).length);

// 			$loadMoreBtn.find('[data-epc-pagination-statistics]').text(items + '/' + total);

// 			if( parseInt(total) <= items ) {
// 				$loadMoreBtn.remove();
// 			}

// 			that.removeSpinner();

// 			return true;

// 		}

// 	};

// 	this.startSpinner = function(){
// 		$loadMoreBtn.prop('disabled', true);
// 		$spinner.css('display', 'inline-block');
// 		$spinner.addClass('epc-show');
// 	};

// 	this.removeSpinner = function(){
// 		$loadMoreBtn.prop('disabled', false);
// 		$spinner.css('display', 'none');
// 		$spinner.removeClass('epc-show');
// 	};

// 	return this.init();

// });
// }


/*
video news from youtube
*/
function onYouTubeIframeAPIReady() {

	var yt_news_fig = qsa('#yt-news figure');

	for(var i = 0; i < yt_news_fig.length;i++) {
		yt_news_fig[i].addEventListener('click', createPlayer);
	}

	function createPlayer() {
		player = new YT.Player(
			this.id, {
				height: this.clientHeight,
				width: this.clientWidth,
				videoId: this.getAttribute('data-yt-id'),
				playerVars: {rel: 0},
				events: {
					'onReady': onPlayerReady
				// 'onStateChange': onPlayerStateChange
			}
		});
	}

	function onPlayerReady(event) {
		event.target.playVideo();
	}

}
/*
video news from youtube /z
*/

/**************
end
define main global function
**************/


/*
Turbolinks event
https://github.com/turbolinks/turbolinks#full-list-of-events
*/
if (Turbolinks.supported) {

	document.addEventListener('turbolinks:load', function(event) {

		MainScriptsOnload();

		if( qs('article.full_article') ){
			BlogArticlePage();
		}

		if( qs('body.c_epc') ){
			EkerlabsPowerContent();
		}

		/****** video news from youtube */
		if( qs('#yt-news') && (typeof(YT)==='undefined') ){
			loadjscssfile('//www.youtube.com/iframe_api', 'js');
		}
		if( qs('#yt-news') && (typeof(YT)!=='undefined') ){
			onYouTubeIframeAPIReady();
		}
		/****** video news from youtube /z*/

		/****** blog items-list infinite scroll */
		if(qs('main>.blog .pagination .dn .pag_next')){
			infiniteScroll( '.blog', '.items_container', '.pagination .dn .pag_next a' );
		}
		/****** blog items-list infinite scroll /z*/

		/****** tags items-list infinite scroll */
		if(qs('main>.tag-category .pagination .dn .pag_next')){
			infiniteScroll( '.tag-category>.form-inline', '.tag_list', '.pagination .dn .pag_next a' );
		}
		/****** tags items-list infinite scroll /z*/

		/****** news cards module infinite scroll */
		if(qs('.ajax_news_cards')){
			infiniteScroll( '.ajx_news>div', '.ajax_news_cards', '.ajax_pagination.dn' );
		}
		/****** news cards module infinite scroll /z*/

		if( qs('.lz') ){
			setTimeout(function() {
				bLazy.revalidate();
			}, 5);
		}

		/* update sidebar menus, like tags list, cats list */
		if (qs('.tagspopular')) {
			updateSideMenu('.tagspopular a');
		}
		/* update sidebar menus, like tags list, cats list /z*/


		if(qs('form.form_inline')){
			var formInline = qsa('form.form_inline');
			for(var i = 0, l = formInline.length; i < l; i++){
				formInit(formInline[i]);
			}
		}

		if(qs('.z3w_jform[data-jform-validate]')){
			var formInline = qsa('.z3w_jform[data-jform-validate]');
			for(var i = 0, l = formInline.length; i < l; i++){
				jformInit(formInline[i]);
			}
		}

		// google analytics for ajax
		/*
		if( typeof(ga) !== 'undefined' ){
			// https://developers.google.com/analytics/devguides/collection/analyticsjs/pages?hl=ru
			// https://developers.google.com/analytics/devguides/collection/analyticsjs/single-page-applications?hl=ru
			// ga('set', 'page', '/new-page.html');
			ga('set', 'page', location.pathname);
			ga('send', 'pageview');
		}
		*/
		// google analytics for ajax/

		// google Gtag for ajax
		if( typeof(gtag) !== 'undefined' ){
			// https://developers.google.com/analytics/devguides/collection/gtagjs/single-page-applications?hl=ru
			gtag('config', gtagID, {'page_path': location.pathname});
		}
		// google analytics for ajax/


		// fb pixel tracking
		if( typeof(fbq) !== 'undefined' ){
			fbq('track', 'PageView');
		}
		// fb pixel tracking/

	});


	// document.addEventListener('turbolinks:request-end', function() {
	// 	updateMenu('#z3_menu a');
	// });
	document.addEventListener('turbolinks:before-render', function() {

		updateMenu('#z3_menu a');

	});

	// https://github.com/turbolinks/turbolinks#full-list-of-events
	document.addEventListener('turbolinks:render', function() {

		if(qs('#header-lang')){
			var botomtLangs = qsa('#botmenu-lang a');
			var headerLangs = qsa('#header-lang a');
			for(var i = 0, l = botomtLangs.length; i < l; i++){
				headerLangs[i].href = botomtLangs[i].href;
			}
		}

	});



	document.addEventListener('turbolinks:visit', function() {

		if( typeof(Waypoint) !== 'undefined' ){
			Waypoint.destroyAll();
		}

		if( qs('.jtt.tooltipstered') ){
			$('.jtt.tooltipstered').tooltipster('destroy');
		}
		if( qs('.jtt-html.tooltipstered') ){
			$('.jtt-html.tooltipstered').tooltipster('destroy');
		}

		if( qs('#disqus_thread iframe') ){
			qs('#disqus_thread').innerHTML = '';
		}

	});

	/* preventing anchor links from turbolinks reloading
	/* if you use e.preventDefault() on anchors, you don't need it */
	// document.addEventListener('turbolinks:click', function(e) {
	// 	if (e.target.getAttribute('href').charAt(0) === '#') {
	// 		// alert(e.data.url);
	// 		// console.log(e.data);
	// 		// window.history.replaceState( {} , '', e.data.url ); // https://github.com/turbolinks/turbolinks/issues/75
	// 		return e.preventDefault();
	// 	}
	// }, false);
	/* preventing anchor links from turbolinks reloading /z*/


	document.addEventListener('turbolinks:before-cache', function() {
		var bd_cl = qs('body').className;
		if (bd_cl.indexOf(' mmo') || bd_cl.indexOf(' z3_modal') || bd_cl.indexOf(' modal_container-visible')) {
			removeCL('mmo', 'body');
			removeCL('z3_modal', 'body');
			removeCL('modal_container-visible', 'body');
		}
	});

	/*
	// https://github.com/turbolinks/turbolinks/pull/325
	// https://github.com/turbolinks/turbolinks/issues/324
	document.addEventListener('turbolinks:after-cache', function() {
		alert();
	});
	*/

} else {

	/*
	window.addEventListener("load", function(event) {
	});
	It is an incredibly common mistake to use 'load' where 'DOMContentLoaded' would be much more appropriate, so be cautious.
	*/
	document.addEventListener('DOMContentLoaded', function(event) {

		// @todo: add copy of turbolinks load event functions

		MainScriptsOnload();

		if( qs('article.full_article') ){
			BlogArticlePage();
		}

		if( qs('body.c_epc') ){
			EkerlabsPowerContent();
		}

		/****** video news from youtube */
		if( qs('#yt-news') && (typeof(YT)==='undefined') ){
			loadjscssfile('//www.youtube.com/iframe_api', 'js');
		}
		if( qs('#yt-news') && (typeof(YT)!=='undefined') ){
			onYouTubeIframeAPIReady();
		}
		/****** video news from youtube /z*/

		/****** blog items-list infinite scroll */
		if(qs('main>.blog .pagination .dn .pag_next')){
			infiniteScroll( '.blog', '.items_container', '.pagination .dn .pag_next a' );
		}
		/****** blog items-list infinite scroll /z*/

		/****** tags items-list infinite scroll */
		if(qs('main>.tag-category .pagination .dn .pag_next')){
			infiniteScroll( '.tag-category>.form-inline', '.tag_list', '.pagination .dn .pag_next a' );
		}
		/****** tags items-list infinite scroll /z*/

		/****** news cards module infinite scroll */
		if(qs('.ajax_news_cards')){
			infiniteScroll( '.ajx_news>div', '.ajax_news_cards', '.ajax_pagination.dn' );
		}
		/****** news cards module infinite scroll /z*/

		if( qs('.lz') ){
			setTimeout(function() {
				bLazy.revalidate();
			}, 5);
		}

		/* update sidebar menus, like tags list, cats list */
		if (qs('.tagspopular')) {
			updateSideMenu('.tagspopular a');
		}
		/* update sidebar menus, like tags list, cats list /z*/

		// @todo: don't need here, because of you invoke in in window onload event
		// // google Gtag for ajax
		// if( typeof(gtag) !== 'undefined' ){
		// 	// https://developers.google.com/analytics/devguides/collection/gtagjs/single-page-applications?hl=ru
		// 	gtag('config', gtagID, {'page_path': location.pathname});
		// }
		// // google analytics for ajax /
	});

}
/*
Turbolinks event /z
https://github.com/turbolinks/turbolinks#full-list-of-events
*/
document.addEventListener('DOMContentLoaded', function(event) {

	updateMenu('#z3_menu a');

	setTimeout(function(){
		var hiddenDropdowns = qsa('#z3_menu > ul > .deep > .hide_ul');
		for(var i = 0, length1 = hiddenDropdowns.length; i < length1; i++){
			var elW = hiddenDropdowns[i].offsetWidth;
			var parElW = hiddenDropdowns[i].parentElement.offsetWidth;
			if (elW > parElW) {
				hiddenDropdowns[i].style.left = '-'+(elW - parElW)/2 +'px';
			}
		}
	}, 1);

});


window.addEventListener('load', function(event) {

	document.documentElement.className += ' tr';

	// _detectDevice variable declare in vendor.js file
	if (!Cookies.get('detect_device')) {
		Cookies.set('detect_device', _detectDevice.replace(/ /g, '+'), {expires: 365});
	}

	// centered dropdowns in header menu
	// if (!Modernizr.touchevents) {
	// 	setTimeout(function(){
	// 		var hiddenDropdowns = qsa('#z3_menu > ul > .deep > ul');
	// 		for(var i = 0, length1 = hiddenDropdowns.length; i < length1; i++){
	// 			var elW = hiddenDropdowns[i].offsetWidth;
	// 			var parElW = hiddenDropdowns[i].parentElement.offsetWidth;
	// 			if (elW > parElW) {
	// 				hiddenDropdowns[i].style.left = '-'+(elW - parElW)/2 +'px';
	// 			}
	// 		}
	// 	}, 10);
	// }

	if (qs('#z3_mobmenu')) {
		qs('#z3_mobmenu').addEventListener('click', mobileMenu);
	}

	if(qs('#fullscreen-mode')){
		qs('#fullscreen-mode').addEventListener('click', toggleFullScreen);
	}

	/***************** онклик по #z3_modal только для род.контейнера */
	if(qs('#modal_container')){
		qs('#modal_container').addEventListener('click', function(event){
			event = event || window.event;
			var t = event.target || event.srcElement;
			if (t != this) { return true; }
			rmCl('body', 'modal_container-visible');
			rmCl(this.querySelector('.modal_child-active'), '.modal_child-active');
		}, false);
	}
	if(qs('#z3_modal')){
		qs('#z3_modal').addEventListener('click', function(event){
			event = event || window.event;
			var t = event.target || event.srcElement;
			if (t != this) { return true; }
			changeCL('z3_modal','body');
		}, false);
	}
	/***************** онклик по #z3_modal только для род.контейнера END*/

	if(qs('#contact-form')){
		qs('#contact-form').addEventListener('click', formAjax);
	}

	/*************** сменить дизайн */
	if (qs('#change-dsgn')) {
		qs('#change-dsgn').addEventListener('click',(function () {

			var desineName = this.getAttribute('data-dsgn');
			// @todo: make it without reload
			if(!Cookies.get('dsgn')){
				Cookies.set('dsgn', desineName, {expires: 365});
				document.location.reload(true);
				// loadjscssfile('/a/'+desineName+'.css', 'css');
				// console.log(jscssfileadded);
			} else {
				Cookies.remove('dsgn');
				// // qs('[href="/a/'+desineName+'.css"]').remove(); // not work in IE
				// var styleLinks = qsa('head [href="/a/'+desineName+'.css"]');
				// for(var i = 0, length1 = styleLinks.length; i < length1; i++){
				// 	styleLinks[i].parentNode.removeChild(styleLinks[i]);
				// }
				// // styleLinks.parentNode.removeChild(styleLinks);
				// jscssfileadded = jscssfileadded.replace('/a/'+desineName+'.css', '');
				// console.log(jscssfileadded);

				document.location.reload(true);
			}
		}));
	}
	/*************** сменить дизайн /z*/

	if (qs('#footer .upleft')) {
		qs('#footer .upleft').addEventListener('click', toTopFunction);
		qs('#footer .upbottom').addEventListener('click', toTopFunction);
	}

	/* google Gtag for ajax */
	if( typeof(gtag) !== 'undefined' ){
		gtag('js', new Date());
		gtag('config', gtagID);
	}
	/* google analytics for ajax /z*/


	// fb pixel init
	if(typeof(fbpix) !== 'undefined'){
		!function(f,b,e,v,n,t,s) {
			if(f.fbq){
				return;
			}
			n=f.fbq=function(){
				n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)
			};
			if(!f._fbq){
				f._fbq=n;
				n.push=n;
				n.loaded=!0;
				n.version='2.0';
			}
			n.queue=[];
			t=b.createElement(e);t.async=!0;
			t.src=v;
			s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)
		}
		(window, document,'script', 'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', fbpix);
		fbq('track', 'PageView');
	}
	// fb pixel init/


	if(qs('#web-development-services')){
		loadjscssfile('/_js/a/animation.js', 'js');
	}


	if (qs('head #prod[data-cpa]') && qs('#side_a')) {
		setTimeout(function() {

			// in case of
			// Cross-Origin Request Blocked: The Same Origin Policy disallows reading the remote resource at https://z3w.host/cpa/. (Reason: CORS header ‘Access-Control-Allow-Origin’ missing)
			// in requested site .htaccess add this - Header set Access-Control-Allow-Origin "*"
			// or replace "*" -> "https://current-site.url"

			// 	loadjscssfile('/_js/a/cpa.js', 'js');
			// }, 10);
			loadjscssfile('https://z3w.host/cpa/min.js', 'js');
		}, 10000);
	}

	if (qs('#header .btn_login_modal')) {
		qs('#header .btn_login_modal').addEventListener('click', _modalLogin);
	}


});


/*************************************************************************
END
FUNCTION CALLS
************************************************************************/


// sessionStorage - только на время сессии клиента, при переходе между страницами
// // Сохранение данных в sessionStorage
// sessionStorage.setItem('key', 'value');
// sessionStorage.setItem('key', 'value');
// // Получение данных из sessionStorage
// var data = sessionStorage.getItem('key');
// // Удаление данных из sessionStorage
// sessionStorage.removeItem('key');

// alert(sessionStorage.getItem('key'));
