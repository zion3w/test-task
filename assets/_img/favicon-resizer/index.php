<?php

// https://github.com/claviska/SimpleImage

include 'SimpleImage.php';

try {
  // Create a new SimpleImage object
	$image = new \claviska\SimpleImage();

  // Magic! ✨
/*
	$image
    ->fromFile('img.png')                     // load image.jpg
    ->autoOrient()                              // adjust orientation based on exif data
    ->resize(512, 512)                          // resize to 320x200 pixels
    ->flip('x')                                 // flip horizontally
    ->colorize('DarkBlue')                      // tint dark blue
    ->border('black', 10)                       // add a 10 pixel black border
    ->overlay('watermark.png', 'bottom right')  // add a watermark image
    ->toFile('new-image.png', 'image/png')      // convert to PNG and save a copy to new-image.png
    ->toScreen();                               // output to the screen
*/
    $image
    ->fromFile('img.png')                     // load image
    ->resize(512, 512)                          // resize
    ->toFile('favicon/favicon512.png', 'image/png')
    ->resize(310, 310)
    ->toFile('favicon/favicon310.png', 'image/png')
    ->resize(270, 270)
    ->toFile('favicon/favicon270.png', 'image/png')
    ->resize(192, 192)
    ->toFile('favicon/favicon192.png', 'image/png')
    ->resize(180, 180)
    ->toFile('favicon/favicon180.png', 'image/png')
    ->resize(167, 167)
    ->toFile('favicon/favicon167.png', 'image/png')
    ->resize(152, 152)
    ->toFile('favicon/favicon152.png', 'image/png')
    ->resize(150, 150)
    ->toFile('favicon/favicon150.png', 'image/png')
    ->resize(144, 144)
    ->toFile('favicon/favicon144.png', 'image/png')
    ->resize(120, 120)
    ->toFile('favicon/favicon120.png', 'image/png')
    ->resize(96, 96)
    ->toFile('favicon/favicon96.png', 'image/png')
    ->resize(72, 72)
    ->toFile('favicon/favicon72.png', 'image/png')
    ->resize(70, 70)
    ->toFile('favicon/favicon70.png', 'image/png')
    ->resize(62, 62)
    ->toFile('favicon/favicon62.png', 'image/png')
    ->resize(48, 48)
    ->toFile('favicon/favicon48.png', 'image/png')
    ->resize(36, 36)
    ->toFile('favicon/favicon36.png', 'image/png')
    ->resize(32, 32)
    ->toFile('favicon/favicon32.png', 'image/png')
    ->resize(16, 16)
    ->toFile('favicon/favicon.png', 'image/png');
  // And much more! 💪
  } catch(Exception $err) {
  // Handle errors
  	echo $err->getMessage();
  }

  ?>