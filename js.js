'use strict';

/* global */
function qs(selector) {
	return document.querySelector(selector);
}
function qsa(selectorAll) {
	return document.querySelectorAll(selectorAll);
}
/* global/ */


document.addEventListener('DOMContentLoaded', function(event) {

	if (localStorage.menuLeftToogler === 'mini') {
		qs('#menu-left-toogle').parentElement.classList.toggle('mini');
	}

	qs('#menu-left-toogle').addEventListener('click', function(e){
		if (localStorage.menuLeftToogler === 'mini') {
			this.parentElement.classList.remove('mini');
			localStorage.menuLeftToogler = '';
		} else {
			this.parentElement.classList.add('mini');
			localStorage.menuLeftToogler = 'mini';
		}
	});

	qs('#log-out-button').addEventListener('click', function(e){
		e.preventDefault;

		var cookies = document.cookie.split(';');

		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i];
			var eqPos = cookie.indexOf('=');
			var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
			document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
		}
		location.reload();
	});

	// chat

	var chatForm = qs('#form-chat');
	var chatTextInput = qs('#chat-form-text');

	function chatFormSend(e) {
		e.preventDefault();

		var formEl = this;
		formEl.querySelector('[type="submit"]').disabled = true;

		var formData = new FormData();
		formData.append(formEl.querySelector('#chat-form-text').name, formEl.querySelector('#chat-form-text').value);

		var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
		xhttp.open('POST', formEl.getAttribute('action'));

		xhttp.onreadystatechange = function () {

			if (xhttp.readyState === 4 && xhttp.status === 200) {

				formEl.querySelector('[type="submit"]').disabled = false;
				formEl.querySelector('#chat-form-text').value = '';
				console.log(xhttp.responseText);

			} else if(xhttp.readyState === 4 && xhttp.status !== 200) {

				qs('#chat-form-resp').innerHTML = '<span class="red">request error, see console</span>';
				console.log(xhttp.responseText);
			}
		}

		xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhttp.send(formData);

	}

	chatForm.addEventListener('submit', chatFormSend);

	// chat polling

	function chatPolling() {

		var chatMessagesContainer = qs('#chat-log');
		var lastHtmlMessageId = chatMessagesContainer.getAttribute('data-last-message');

		fetch('/chat-get-messages.php?last_message_id='+lastHtmlMessageId)
		.then((response) => {
			return response.json();
		})
		.then((data) => {

			if (data.length > 0) {
				var lastDataMessageId = data[data.length-1]['id'];

				if (lastDataMessageId > lastHtmlMessageId) {

					var chatMessagesHtml = '';

					data.forEach( function(message, index) {
						chatMessagesHtml += '<div class="chat_log_message" data-id="'+message.id+'"><span class="chat_log_message_user">'+message.uname+'</span>: <span class="chat_log_message_text">'+message.message+'</span></div>';
					});
					chatMessagesContainer.innerHTML += chatMessagesHtml;
					chatMessagesContainer.setAttribute('data-last-message', lastDataMessageId);
					chatMessagesContainer.scrollTop = chatMessagesContainer.scrollHeight;
				}
			}


		});

	}
	var chatPollingInterval = setInterval(chatPolling, 1000);

	// chat polling/

	// chat/




	// timer

	var timerObj = {
		status: 'stop',
		time: 0,
		timeStart: 0,
		timeStop: 0,

		buttonStart: qs('#timer-start'),
		buttonStop: qs('#timer-stop'),

		initTimer(){
			fetch('/get-data.php/?auth=ts@2020&format=json&table=timer&columns=*')
			.then((response) => {
				return response.json();
			})
			.then((data) => {
				var data1 = data[0];
				timerObj.time = parseInt(data1['time']);
				timerObj.timeStart = parseInt(data1['time_start']);
				timerObj.timeStop = parseInt(data1['time_stop']);

				if (data1['status'] === 'start') {
					console.log('time_diff: ' + parseInt(data['time_diff']));
					timerObj.time = parseInt(data['time_diff']);
				}

				if (data1['status'] === 'stop') {
					timerObj.stopTimer();
				}

				timerObj.setTimer();

			})
		},
		pollingTimerStatus(){
			fetch('/get-data.php/?auth=ts@2020&format=json&table=timer&columns=*')
			.then((response) => {
				return response.json();
			})
			.then((data) => {
				var data1 = data[0];
				if (timerObj.status !== data1['status']) {
					timerObj.status = data1['status'];
				}
			})
		},
		stopTimer(){
			timerObj.buttonStop.disabled=true;
			timerObj.buttonStart.disabled = false;

			clearInterval(timerObj.setTimeInterval);

		},
		startTimer(){
			timerObj.buttonStop.disabled=false;
			timerObj.buttonStart.disabled = true;

			timerObj.setTimeInterval = setInterval(function(){
				timerObj.setTimer();
			}, 1000);

		},

		secondsDomNode: qs('#timer-seconds'),
		minutesDomNode: qs('#timer-minutes'),
		hoursDomNode: qs('#timer-hours'),

		setTimer(){
			++timerObj.time;
			// console.log(timerObj.time);
			var totalMinutes = parseInt(timerObj.time / 60);
			var totalHours = parseInt(totalMinutes / 60);

			timerObj.secondsDomNode.innerHTML = timerObj.countTime(parseInt(timerObj.time) % 60);
			timerObj.minutesDomNode.innerHTML = timerObj.countTime(parseInt(totalMinutes % 60));
			timerObj.hoursDomNode.innerHTML = timerObj.countTime(parseInt(totalMinutes / 60));

		},
		countTime(val) {
			var valString = val + '';
			if (valString.length < 2) {
				return '0' + valString;
			} else {
				return valString;
			}
		},
	};
	// https://learn.javascript.ru/proxy
	timerObj = new Proxy(timerObj, {
		set(target, prop, val) {

			if (prop === 'status') {

				if (val === 'stop' && timerObj.status === 'start') {
					// console.log('stop-timerObj.status:' + timerObj.status);
					timerObj.stopTimer();
				} else if (val === 'start' && timerObj.status === 'stop') {
					// console.log('start-timerObj.status:' + timerObj.status);
					timerObj.startTimer();
				} else {

				}
			}
			target[prop] = val;

			return true;
		}
	});

	// timerObj.time = (3600 * 2) - 4;

	// timerObj.setTimer();
	timerObj.initTimer();

	timerObj.pollingInterval = setInterval(function(){
		timerObj.pollingTimerStatus();
	}, 1000);



	document.querySelector('#timer-start').addEventListener('click', function (e) {

		fetch('/timer-status.php/?start='+timerObj.time)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			timerObj.status = 'start';
		});
	});

	document.querySelector('#timer-stop').addEventListener('click', function (e) {

		fetch('/timer-status.php/?stop='+timerObj.time)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			timerObj.status = 'stop';
		});
	});


});


// window.onbeforeunload = function (e) {

// 	e = e || window.event;
// 	// For IE and Firefox prior to version 4
// 	if (e) {
// 		e.returnValue = 'Sure?';
// 	}
// 	// For Safari
// 	return 'Sure?';

// };

