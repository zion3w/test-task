<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// set_time_limit(0);


require_once __DIR__ . '/php/helper.php';
$helper = new Helper();

require (__DIR__ . '/config.php');



if (isset($_POST['sign-up-email'])) {

	$filteredArr = [];

	foreach ($_POST as $key => $value) {
		$key = filter_var($key, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);
		$value = filter_var($value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		$filteredArr[$key] = $value;

	};


	$filteredArr['sign-up-email'] = filter_var($filteredArr['sign-up-email'], FILTER_SANITIZE_EMAIL);


	$userTypes = [
		0 => 'guest',
		1 => 'registered',
		2 => '',
		3 => '',
		4 => '',
		5 => 'manager',
		6 => '',
		7 => '',
		8 => '',
		9 => 'admin',
	];

	$userData = [
		'ip' => $helper->getUserIP(),
		'user_agent' => $_SERVER['HTTP_USER_AGENT'],
	];

	$user = [
		'username' => $filteredArr['sign-up-email'],
		'email' => $filteredArr['sign-up-email'],
		'password' => password_hash($filteredArr['sign-up-password'], PASSWORD_DEFAULT),
		'usertype' => 1,
		'name' => $filteredArr['sign-up-name'],
		'token' => '',
		'info' => '',
		'data' => json_encode($userData),
		'data_log' => json_encode($userData),
	];

	$user['username'] = substr($user['username'], 0, strpos($user['username'], '@')) . '_' . time();


	$dbTableName = 'users';

	try {

		$db->insertInto($dbTableName, $user);

		header('Location: /sign-in.php');

		return;

	} catch (Exception $e) {

		http_response_code(400);
		echo '<h1>Error: <b style="color:#9c0101">"insertInto"</b> <i>catch Exception: </i><br><pre><code>' . $e . '</code></pre></h1>' .  "\n";
		// echo 'insertInto error Exception: ' . $e->getMessage() . "\n";

	}

	/*
	id | username | email | password | usertype | name | token | info | data | data_log | date_reg | date_log
	*/

}
?>




<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>sign up</title>
	<link rel="stylesheet" type="text/css" href="css.css">
	<link rel="stylesheet" type="text/css" href="test.css">
</head>
<body>


	<h1>Sign Up</h1>

	<section id="sign-up" class="tac">
		<div class="dib tal">

			<form id="sign-up-form" class="form_signup" method="post" action="">

				<div class="form_signup_field">
					<label for="sign-up-name">Введите Имя</label>
					<input id="sign-up-name" name="sign-up-name" placeholder="Ваше имя" type="text" maxlength="50" minlength="3" required tabindex="1">
				</div>

				<div class="form_signup_field">
					<label for="sign-up-email">Введите Email</label>
					<input id="sign-up-email" name="sign-up-email" placeholder="Электронная почта" type="email" maxlength="50" minlength="7" required tabindex="2">
				</div>

				<div class="form_signup_field">
					<label for="sign-up-password">Введите Пароль</label>
					<input id="sign-up-password" name="sign-up-password" placeholder="Пароль" type="password" maxlength="50" minlength="5" required tabindex="3">
				</div>

				<div class="form_signup_field">
					<button id="sign-up-submit" class="btn i_ok" type="submit" tabindex="4">Зарегистрироваться</button>
				</div>

				<div class="form_signin_field">
					<h3>
						<a href="/sign-in.php">Войти</a>
					</h3>
				</div>

			</form>

		</div>
	</section>

</body>
</html>
