<?php // variables and configurations

// @info: set default time zone
date_default_timezone_set('Europe/Kiev');
// @info: to get all timezon abbreviation list, use
// $timezone_abbreviations = DateTimeZone::listAbbreviations();
// print_r($timezone_abbreviations);


// @info: set date output format
// https://www.php.net/manual/ru/function.setlocale.php
setlocale(LC_ALL, 'rus_RUS.UTF8');

// how to output localized dates
// https://www.php.net/manual/ru/function.strftime.php
// echo strftime('%A %e %B %Y %H:%M:%S', filemtime(__FILE__));

// template variables

$config = [

	'domain' => 'test-task.dev',

	'db' => [
		'host' => 'localhost',
		'user' => 'mysql',
		'pass' => 'mysql',
		'db_name' => 'test_task',
	],


];

$config['auth'] = [
	'auth_secret' => md5($config['domain'] . @$_SERVER['HTTP_ACCEPT_LANGUAGE']),
	// 'auth_secret' => $config['domain'] . @$_COOKIE['detect_device'] . $helper->getUserIP(),
];


require (__DIR__ . '/php/dbController.php');

$db = new DbController(
	$config['db']['host'],
	$config['db']['user'],
	$config['db']['pass'],
	$config['db']['db_name']
);
