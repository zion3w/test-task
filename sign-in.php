<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// set_time_limit(0);



require_once __DIR__ . '/php/helper.php';
$helper = new Helper();

require (__DIR__ . '/config.php');



if (isset($_POST['sign-in-login']) && isset($_POST['sign-in-password'])) {




	$filteredArr = [];

	foreach ($_POST as $key => $value) {
		$key = filter_var($key, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);
		$value = filter_var($value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		$filteredArr[$key] = $value;

	};

	$userLogin = $filteredArr['sign-in-login'];
	$userPass = $filteredArr['sign-in-password'];


	$userLoginByField = 'username';

	if (strpos($userLogin, '@') !== false) {
		$userLoginByField = 'email';
	}

	$dbTableName = 'users';
	$userColumns = "id,username,email,password,usertype,name";
	// id | username | email | password | usertype | name | token | info | data | data_log | date_reg | date_log


	$userColumnsArr = $db->query("SELECT $userColumns FROM $dbTableName WHERE $userLoginByField = '$userLogin'");


	if (isset($userColumnsArr[0]) && password_verify($userPass, $userColumnsArr[0]['password'])) {
		$user = $userColumnsArr[0];

		// cookies for restoring user session
		setcookie(
			$config['auth']['auth_secret'],
			json_encode([
				$helper->base64EncodeSafe('id') => $helper->base64EncodeSafe($user['id']),
				$helper->base64EncodeSafe('pass') => md5($user['password']),
			]),
			time()+(60*60*24*360), // 1 year user session
			'/'
		);

		session_start();

		unset($user['password']);

		$_SESSION['user'] = $user;

		header('Location: /');

		return;

	} else {
		$signError = '<h2 class="red">Wrong password or User not exists</h2>';
	}




}




?>







<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>sign up</title>
	<link rel="stylesheet" type="text/css" href="css.css">
	<link rel="stylesheet" type="text/css" href="test.css">
</head>
<body>

	<h1>Sign In</h1>

	<section id="sign-in" class="tac">

		<?php
		if (isset($signError)) {
			echo $signError;
		}
		?>
		<div class="dib tal">

			<form id="sign-in-form" class="form_signin" method="post" action="">

				<div class="form_signin_field">
					<label for="sign-in-login">Введите Логин или Email</label>
					<input id="sign-in-login" name="sign-in-login" placeholder="Логин или электронная почта" type="text" maxlength="50" minlength="3" required tabindex="1">
				</div>

				<div class="form_signin_field">
					<label for="sign-in-password">Введите Пароль</label>
					<input id="sign-in-password" name="sign-in-password" placeholder="Пароль" type="password" maxlength="50" minlength="5" required tabindex="2">
				</div>

				<div class="form_signin_field">
					<button id="sign-in-submit" class="btn i_ok" type="submit" tabindex="3">Войти</button>
				</div>
				<div class="form_signin_field">
					<h3>
						<a href="/sign-up.php">Зарегистрироваться</a>
					</h3>
				</div>

			</form>

		</div>
	</section>

</body>
</html>
