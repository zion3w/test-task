<?php

if (!isset($_GET['last_message_id'])) {
	die();
}

$lastMessageId = filter_var($_GET['last_message_id'], FILTER_SANITIZE_NUMBER_INT);

require_once __DIR__ . '/php/helper.php';
$helper = new Helper();

require (__DIR__ . '/config.php');


$dbTableName = 'chat';

$columns = '*';

$allColumnsArr = $db->query("SELECT $columns FROM $dbTableName WHERE id > $lastMessageId");

header('Content-Type: application/json');
echo json_encode($allColumnsArr);
