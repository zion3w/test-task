// 'use strict';
// https://www.youtube.com/playlist?list=PLDyvV36pndZFLTE13V4qNWTZbeipNhCgQ
const gulp = require('gulp'),
fs = require('fs'),
path = require('path'),
rename = require('rename'), // npm install rename --save-dev
concat = require('gulp-concat'), //npm i -D gulp-concat // -D same as --save-dev
debug = require('gulp-debug'), //npm install --save-dev gulp-debug
gulpRename = require('gulp-rename'), // npm i -D gulp-rename
plumber = require('gulp-plumber'),
notify = require('gulp-notify'),
browserSync = require('browser-sync').create(),
// bsReload = browserSync.reload, //don't need for now


changed = require('gulp-changed'),
cached = require('gulp-cached'),
lessChanged = require('gulp-less-changed'),

concatCss = require('gulp-concat-css'),
groupCSSmq = require('gulp-group-css-media-queries'),
postcss = require('gulp-postcss'),
autoprefixer = require('autoprefixer'), // npm i -D postcss-preset-env
cleanCss = require('gulp-clean-css'),

less = require('gulp-less'), // npm i -D gulp-less
lessBaseImport = require('gulp-less-base-import'),
sourcemaps = require('gulp-sourcemaps'), // npm i -D gulp-sourcemaps

uglify = require('gulp-uglify');



// php+js+less live reload+dev: tasks
gulp.task('watch:live', function() {
// gulp.task('dev', function() {
	var currentDir = __dirname.split("\\");
	currentDir = 'https://' + currentDir[currentDir.length - 1];
	browserSync.init({
		open: false,
		notify: false,
		logLevel: 'warn',
		logFileChanges: false,
		ghostMode: {
			clicks: false,
			forms: true,
			scroll: false
		},
		snippetOptions: {
			rule: {
				async: true,
				match: /<\/head>/i,
				fn: function (snippet, match) {
					return snippet + match;
				}
			}
		},
		proxy: currentDir,
		online: false,
		https: {
			key: "./_ssl_localhost/localhost.key",
			cert: "./_ssl_localhost/localhost.crt"
		},
		files: [
		'*.php',
		'php/**/*.php',
		'*.js',
		'*.css',
		],
	});
});
gulp.task('watch', gulp.series('watch:live'));
/*******************************************
development tasks end
*******************************************/

