-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 23, 2020 at 09:24 AM
-- Server version: 10.3.13-MariaDB-log
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `uname` varchar(350) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_stamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `uid`, `uname`, `message`, `time_stamp`) VALUES
(24, 10, 'egfsdfg', '88888888', '2020-03-22 19:06:41'),
(25, 10, 'egfsdfg', '5555555555555555555', '2020-03-22 19:07:48'),
(26, 10, 'egfsdfg', '9999999999999', '2020-03-22 19:08:58'),
(27, 10, 'egfsdfg', '999999999999999', '2020-03-22 19:09:39'),
(28, 10, 'egfsdfg', '8888888888888', '2020-03-22 19:10:11'),
(29, 10, 'egfsdfg', '1111111111111111111', '2020-03-22 19:12:19'),
(30, 10, 'egfsdfg', '88888888888', '2020-03-22 19:16:15'),
(31, 10, 'egfsdfg', '9999999999', '2020-03-22 19:29:32'),
(32, 10, 'egfsdfg', '44444444444', '2020-03-22 19:29:37'),
(33, 10, 'egfsdfg', '8888888888', '2020-03-22 19:31:36'),
(34, 10, 'egfsdfg', 'sdf sdfsd f', '2020-03-22 19:31:38'),
(35, 10, 'egfsdfg', 's fds fds', '2020-03-22 19:31:39'),
(36, 10, 'egfsdfg', '23423423423', '2020-03-22 19:32:30'),
(37, 10, 'egfsdfg', '999999999', '2020-03-22 19:32:38'),
(38, 10, 'egfsdfg', '8888888888888888', '2020-03-22 19:35:08'),
(39, 10, 'egfsdfg', '999999', '2020-03-22 19:35:30'),
(40, 10, 'egfsdfg', '8888888888', '2020-03-22 19:35:46'),
(41, 10, 'egfsdfg', 'rrrrrrrrrrrr', '2020-03-22 20:07:19'),
(42, 10, 'egfsdfg', '888888888', '2020-03-22 20:08:06'),
(43, 10, 'egfsdfg', '2323sd fsdf sdf', '2020-03-22 20:13:57'),
(44, 10, 'egfsdfg', 'dfg dfg dfg d', '2020-03-22 20:15:02'),
(45, 10, 'egfsdfg', '111111', '2020-03-22 20:15:15'),
(46, 10, 'egfsdfg', '12', '2020-03-22 20:15:29'),
(47, 10, 'egfsdfg', 'sdfsdf', '2020-03-23 06:06:12'),
(48, 11, '1111', '234234234', '2020-03-23 06:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `timer`
--

CREATE TABLE `timer` (
  `id` int(10) NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_start` int(11) NOT NULL,
  `time_stop` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `time_stamp` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `timer`
--

INSERT INTO `timer` (`id`, `status`, `time_start`, `time_stop`, `time`, `time_stamp`) VALUES
(1, 'stop', 1584943607, 1584943626, 18, '2020-03-22 15:23:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` int(10) NOT NULL,
  `name` varchar(350) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_log` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_reg` datetime NOT NULL DEFAULT current_timestamp(),
  `date_log` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `usertype`, `name`, `token`, `info`, `data`, `data_log`, `date_reg`, `date_log`) VALUES
(1, 'w_1584802957', 'w@ww.ww', '111111', 1, '9999&lt;&gt;&#039;`sdf', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '2020-03-21 17:02:37', '2020-03-21 17:02:37'),
(3, '_1584804729', '', '$2y$10$m62Lv0cUWKu.NerOsC9jeOgKLsJ/VnjHGw8CXsKIQ2oShYnn8n.oG', 1, '', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '2020-03-21 17:32:09', '2020-03-21 17:32:09'),
(7, 'w7_1584805108', 'w7@ww.ww', '$2y$10$htVIkcQ6mTuFff53.I4t7OVlwgH6LiM4IYDwJmC/DGIBCsQotvXCy', 1, '9999&lt;&gt;&#039;`sdf', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '2020-03-21 17:38:28', '2020-03-21 17:38:28'),
(10, 'w1_1584805283', 'w1@ww.ww', '$2y$10$jnsmhAwgpdBFnwTw7Vax3eqtxt59Tk0BCVFCAmmV5qOjV0Zg4P4Sq', 1, 'egfsdfg', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '2020-03-21 17:41:23', '2020-03-21 17:41:23'),
(11, 'w_1584814342', 'w@w77.7w', '$2y$10$5ClMSlxU2drhFt93h2umOuqN7b3FZEaXEbY2cB2LTkOKXHYBsvpt2', 1, '1111', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko\\/20100101 Firefox\\/75.0\",\"cookies\":\" firefox windows desktop no-touchevents\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko\\/20100101 Firefox\\/75.0\",\"cookies\":\" firefox windows desktop no-touchevents\"}', '2020-03-21 20:12:22', '2020-03-21 20:12:22'),
(13, 'w19_1584871082', 'w19@ww.ww', '$2y$10$nrHQhHbodPSTvD3VvvWRGOHgV/0JCeYdzwMIfcYoACf3JhBHv5RLS', 1, '99999hhh', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":\" chrome windows desktop no-touchevents\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":\" chrome windows desktop no-touchevents\"}', '2020-03-22 11:58:02', '2020-03-22 11:58:02'),
(15, 'w551_1584871173', 'w551@ww.ww', '$2y$10$nPbi9zgYtpuCTfbH2Q1uP.j5FKamxpjR/GFxCvDIdfVYupvMOKRKe', 1, '555', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":\" chrome windows desktop no-touchevents\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":\" chrome windows desktop no-touchevents\"}', '2020-03-22 11:59:33', '2020-03-22 11:59:33'),
(16, 'w1jgf_1584871300', 'w1jgf@ww.ww', '$2y$10$yuFLNZcHariX.6qZWHk40OSevvD8RJP9zR4s1aBRcr9cykI357sJq', 1, '8545522', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":\" chrome windows desktop no-touchevents\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":\" chrome windows desktop no-touchevents\"}', '2020-03-22 12:01:40', '2020-03-22 12:01:40'),
(17, 'w199_1584872744', 'w199@ww.ww', '$2y$10$MJek0tWa61eyKkKhHtnTruoSm.l7GeQ5Wq4dm0UVu0SYC8oxZGwqm', 1, 'uuuuu99', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":null}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\",\"cookies\":null}', '2020-03-22 12:25:44', '2020-03-22 12:25:44'),
(18, 'w19999_1584872855', 'w19999@ww.ww', '$2y$10$HMVy9csRWO2X48JAODa/7.H4aec.BEfbooBHqoatqnGzEQ/gfu0I6', 1, '9999999', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '2020-03-22 12:27:35', '2020-03-22 12:27:35'),
(19, 'w155555_1584872894', 'w155555@ww.ww', '$2y$10$IFSUX99bA/8Id9uj2qY60.ylADavncIHDReE.Q9b9eqS4LbB9cjim', 1, '55555', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '2020-03-22 12:28:14', '2020-03-22 12:28:14'),
(20, 'w1_1584872954', 'w1@999999ww.ww', '$2y$10$XaKF1mb1vm/HqElyPkWAkeMvrqicMV8RrIfd2mQsIlhqHl3rq1GSS', 1, '9999999999999', '', '', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '{\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/81.0.4016.0 Safari\\/537.36\"}', '2020-03-22 12:29:14', '2020-03-22 12:29:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timer`
--
ALTER TABLE `timer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `timer`
--
ALTER TABLE `timer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
