<?php

class Helper
{

	public function generateMenu($routes)
	{

		$menuHeader = '';
		foreach ($routes as $routeUrl => $routeArr) {

			if ($routeUrl === '') {
				$routeUrl = '/';
			}
			if ($routeUrl === '/hidden') {
				continue;
			}

			if (isset($routeArr['nested'])) {
				$menuHeader .= '<li class="deep">';
			} else {
				$menuHeader .= '<li>';
			}

			if (isset($routeArr['attr'])) {
				$menuHeader .= '<a href="'.$routeUrl.'" '.$routeArr['attr'].'><span>'.$routeArr['name'].'</span></a>';
			} else {
				$menuHeader .= '<a href="'.$routeUrl.'" role="menuitem">'.$routeArr['name'].'</a>';
			}

			if (isset($routeArr['nested'])) {
				$menuHeader .= '<ul class="hide_ul">';
				$menuHeader .= $this->generateMenu($routeArr['nested']);
				$menuHeader .= '</ul>';
			}

			$menuHeader .= '</li>';
		}

		return $menuHeader;

	}


	public function generateBootstrapNavMenu($routes)
	{
		// https://getbootstrap.com/docs/4.3/examples/navbars/
		$menuHeader = '';
		$i = 0;
		foreach ($routes as $routeUrl => $routeArr) {

			if ($routeUrl === '') {
				$routeUrl = '/';
			}
			if ($routeUrl === '/hidden') {
				continue;
			}

			// if (isset($routeArr['nested'])) {
			// 	$menuHeader .= '<li class="nav-item dropdown">';
			// } else {
			// 	$menuHeader .= '<li class="nav-item">';
			// }
			$menuHeader .= '<li class="nav-item">';

			// if (isset($routeArr['attr'])) {
			// 	$menuHeader .= '<a href="'.$routeUrl.'" '.$routeArr['attr'].'><span>'.$routeArr['name'].'</span></a>';
			// } else {
			// 	$menuHeader .= '<a href="'.$routeUrl.'" role="menuitem">'.$routeArr['name'].'</a>';
			// }
			$menuHeader .= '<a href="'.$routeUrl.'" class="nav-link">'.$routeArr['name'].'</a>';

			if (isset($routeArr['nested'])) {
				$i++;
				$menuHeader .= '
				<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="dropdown'.$i.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
				<div class="dropdown-menu" aria-labelledby="dropdown'.$i.'">';
				$menuHeader .= $this->generateBootstrapNavMenuDropdownItems($routeArr['nested']);
				$menuHeader .= '</div></li>';
			}

			$menuHeader .= '</li>';
		}

		return $menuHeader;

	}

	public function generateBootstrapNavMenuDropdownItems($routes)
	{
		$menuHeaderDropdownItems = '';

		foreach ($routes as $routeUrl => $routeArr) {

			if ($routeUrl === '') {
				$routeUrl = '/';
			}
			if ($routeUrl === '/hidden') {
				continue;
			}

			$menuHeaderDropdownItems .= '<a href="'.$routeUrl.'" class="dropdown-item">'.$routeArr['name'].'</a>';


		}

		return $menuHeaderDropdownItems;
	}



	public function flattenRoutesArr($arr) {
		$flatArr = [];

		foreach ($arr as $arrKey => $arrVal) {

			$flatArr[$arrKey] = $arrVal['page'];

			if (isset($arrVal['nested'])) {
				$flatArr = array_merge($flatArr, $this->flattenRoutesArr($arrVal['nested']));
			}
		}

		return $flatArr;
	}



	public function arrToHtml($arr)
	{
		$arrHtml = '<div>';
		foreach ($arr as $key => $value) {
			$arrHtml .= '<p><i>'.$key.': </i><br>';
			if (is_array($value)) {
				$this->arrToHtml($value);
			} else {
				$arrHtml .= '<b>'.$value.'</b></p>
				';
			}
		}
		$arrHtml .= '</div>';

		return $arrHtml;
	}

	public function getTimeFileModified_ISO8601($pathToFile)
	{
		$date = filemtime(__FILE__);
		return date('Y-m-d', $date) . 'T' . date('H:i:sO', $date);
	}

	public function getUserIP()
	{
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)){
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
			return $_SERVER['REMOTE_ADDR'];
		}else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
			return $_SERVER['HTTP_CLIENT_IP'];
		}

		return '';
	}

	public function base64EncodeSafe($string)
	{
		return strtr(base64_encode($string), '+/=', '._-');
	}
	public function base64DecodeSafe($string)
	{
		return base64_decode(strtr($string, '._-', '+/='));
	}

}
?>
