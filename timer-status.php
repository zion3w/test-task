<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// set_time_limit(0);

function dd($code){
	echo '<hr><div><pre><code>';
	var_dump($code);
	echo '</code></pre></div><hr>';
	die;
}

$getArr = [];
foreach ($_GET as $key => $value) {

	$key = filter_var($key, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_SANITIZE_STRING);
	$value = filter_var($value, FILTER_SANITIZE_NUMBER_INT);

	$getArr[$key] = $value;
}


require_once __DIR__ . '/php/helper.php';
$helper = new Helper();

require (__DIR__ . '/config.php');


$dbTable = 'timer';

$curTime = time();

if (isset($getArr['stop'])) {

	$timerCurTime = $getArr['stop'];

	$fieldsToUpdate = "status='stop', time_stop='$curTime', time='$timerCurTime'";

	try {
		// $db->query("UPDATE $dbTable SET $fieldsToUpdate WHERE id = '1'");
		$db->query("UPDATE $dbTable SET $fieldsToUpdate ORDER BY id ASC LIMIT 1");
	} catch (Exception $e) {
		echo $e->getMessage();
	}
}
if (isset($getArr['start'])) {

	$timerCurTime = $getArr['start'];

	$fieldsToUpdate = "status='start', time_start='$curTime', time='$timerCurTime'";

	try {
		// $db->query("UPDATE $dbTable SET $fieldsToUpdate WHERE id = '1'");
		$resp = $db->query("UPDATE $dbTable SET $fieldsToUpdate ORDER BY id ASC LIMIT 1");
		// $db->query("UPDATE $dbTable SET $fieldsToUpdate ORDER BY id ASC LIMIT 1");
	} catch (Exception $e) {
		echo $e->getMessage();
	}

}


try {
	$allColumnsArr = $db->query("SELECT * FROM $dbTable");
	header('Content-Type: application/json');
	echo json_encode($allColumnsArr[0]);
} catch (Exception $e) {
	echo $e->getMessage();
}
?>
