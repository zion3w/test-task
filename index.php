<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// set_time_limit(0);

function dd($code){
	echo '<hr><div><pre><code>';
	var_dump($code);
	echo '</code></pre></div><hr>';
	die;
}


require_once __DIR__ . '/php/helper.php';
$helper = new Helper();

require (__DIR__ . '/config.php');


session_start();

if (!isset($_SESSION['user'])) {

	// get new user session from cookies
	if (isset($_COOKIE[$config['auth']['auth_secret']])) {

		$userAuthCookiesArr = json_decode($_COOKIE[$config['auth']['auth_secret']], true);

		$userId = $helper->base64DecodeSafe($userAuthCookiesArr[$helper->base64EncodeSafe('id')]);
		$userPassMd5 = $userAuthCookiesArr[$helper->base64EncodeSafe('pass')];


		$dbTableName = 'users';
		$userColumns = "id,username,email,password,usertype,name";
		$userColumnsArr = $db->query("SELECT $userColumns FROM $dbTableName WHERE id = '$userId'");

		if ($userPassMd5 === md5($userColumnsArr[0]['password'] )) {

			unset($userColumnsArr[0]['password']);
			$_SESSION['user'] = $userColumnsArr[0];
		}

	} else {

		header('Location: /sign-in.php');
		// $userNeedToSingIn = true;
	}

}

$user = $_SESSION['user'];


?>


<!DOCTYPE html>
<html lang="en" class="tr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>test task</title>
	<link rel="stylesheet" type="text/css" href="/css.css">
	<link rel="stylesheet" type="text/css" href="/test.css">
</head>
<body>

	<header id="header" class="header">

		<div class="w100 dfwsbc">
			<a href="/">TestTask</a>
			<div class="user_menu">
				<a class="i_user" href="#header-user"><?php echo $user['name']; ?></a>
				<ul class="user_menu_hiden lsn">
					<li><a id="log-out-button" href="/">Log Out</a></li>
				</ul>
			</div>
		</div>
	</header><!-- /header -->

	<div id="t131">

		<div class="menu_left">
			<button id="menu-left-toogle" class="i_chev_l menu_left_toogle" type="button"></button>
			<ul class="lsn">
				<li>
					<a class="i_hand" href="/?hi"><span>Welcome page</span></a>
				</li>
				<li>
					<a class="i_user" href="/?edit-name"><span>Edit name</span></a>
				</li>
				<li>
					<a class="i_lock" href="/?edit-password"><span>Edit password</span></a>
				</li>
			</ul>
		</div>

		<main id="main">

			<h4>Chat & Timer</h4>

			<section class="dfwsac">

				<div id="chat" class="chat">

					<div id="chat-log" class="chat_log" data-last-message="0"></div>

					<div id="chat-form">
						<form id="form-chat" class="chat_form" method="post" action="/chat-submit.php" autocomplete="off">
							<input id="chat-form-text" name="chat-form-text" type="text" placeholder="&#xe057; message..." minlength="2" required tabindex="1">
							<button id="chat-form-submit" class="btn i_comment" type="submit" tabindex="2">Send</button>
						</form>
						<p id="chat-form-resp"></p>
					</div>
				</div>
				<div id="timer" class="timer">
					<div class="timer_started">
						<p>timer started <span id="timer-time" class="timer_time"><span id="timer-hours">00</span>:<span id="timer-minutes">00</span>:<span id="timer-seconds">00</span></span></p>
					</div>
					<div id="timer-buttons">
						<button id="timer-start" class="btn i_ok" type="button">Start</button>
						<button id="timer-stop" class="btn i_no" type="button">Stop</button>
					</div>

				</div>

			</section>

		</main>

	</div>

	<script src="js.js"></script>
</body>
</html>

